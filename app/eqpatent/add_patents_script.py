

# curl -X GET "localhost:9200/dependant-claim-index/_count?pretty"

##########################################
#### To run the file: ####################
#### exec(open('add_patents_script.py').read()) ####
##########################################

from deep_search.models import Patent, Claim

from bs4 import BeautifulSoup
import os
import re

data_dir = '../Dataset'
folder_dirs = ['I20181204', 'I20181211']

patent_n = 0

def get_title(patent_dir):

	handler = open(patent_dir).read()
	soup = BeautifulSoup(handler, "xml")
	title = None
	if soup.find('invention-title'):
		title = soup.find('invention-title').text
	return title

def get_publ_number(patent_dir):
	handler = open(patent_dir).read()
	soup = BeautifulSoup(handler, "xml")
	publ_number = None
	if soup.find('us-patent-grant'):
		publ_number = soup.find('us-patent-grant').attrs['file']
		publ_number = publ_number.split('.')[0]
	return publ_number

def get_ipc(patent_dir):

	handler = open(patent_dir).read()
	soup = BeautifulSoup(handler, "xml")

	ipc = None
	if soup.find("classification-ipcr"):
		ipc = ""
		ipc += soup.find("classification-ipcr").find('classification-level').text
		ipc += soup.find("classification-ipcr").find('section').text
		ipc += soup.find("classification-ipcr").find('class').text
		ipc += soup.find("classification-ipcr").find('subclass').text
		ipc += soup.find("classification-ipcr").find('main-group').text
		ipc += '/'
		ipc += soup.find("classification-ipcr").find('subgroup').text

	return ipc

def get_applicants(patent_dir):

	handler = open(patent_dir).read()
	soup = BeautifulSoup(handler, "xml")
	applicants = []
	if soup.find('us-applicant'):
		applicants_soup = soup.findAll('us-applicant')

		for applicant in applicants_soup:
			if applicant.find('orgname'):
				applicant_name = applicant.find('orgname').text
				applicants.append(applicant_name)
			elif applicant.find('first-name') and applicant.find('last-name'):
				applicant_name = applicant.find('first-name').text + ' ' + applicant.find('last-name').text
		
		applicants = ','.join(applicants)
		applicants = applicants

		return applicants

def get_date(patent_dir):
	handler = open(patent_dir).read()
	soup = BeautifulSoup(handler, "xml")
	date = None
	if soup.find('us-patent-grant'):
		date = soup.find('us-patent-grant').attrs['date-publ']
		date = date[6] + date[7] + '/' + date[4] + date[5] + '/' + date[0:4]
	return date


def get_claims(patent_dir):

	claims = []
	handler = open(patent_dir).read()
	soup = BeautifulSoup(handler, 'lxml')
	if soup.find('claims'):
		for claim in soup.findAll('claim'):	
			claim = claim.text.replace('\n', ' ')
			claim = re.sub(' +', ' ', claim)

			claims.append(claim)
			
	return claims

def is_independant(claim_text):
	if "claim" in claim_text:
		return False
	return True

def parse_patent(patent_dir):
	patent_title = get_title(patent_dir)
	patent_publ_number = get_publ_number(patent_dir)
	patent_ipc = get_ipc(patent_dir)
	patent_applicants = get_applicants(patent_dir)
	patent_date = get_date(patent_dir)

	patent_claims = get_claims(patent_dir)
	
	return patent_title, patent_publ_number, patent_ipc, patent_applicants, patent_date, patent_claims

def add_patent_to_db(patent_title, patent_publ_number, patent_ipc, patent_applicants, patent_date, patent_claims):
	
	new_patent = Patent(title=patent_title,
	 					publication_number = patent_publ_number,
						ipc = patent_ipc,
						applicants = patent_applicants, 
						date = patent_date)
	new_patent.save()

	for patent_claim in patent_claims:
		new_claim = Claim(text=patent_claim)
		new_claim.patent = new_patent
		new_claim.is_independant = is_independant(patent_claim)
		new_claim.save()

for folder_dir in folder_dirs:
	
	for patent_folder in os.listdir(data_dir+'/'+folder_dir):
		patent_dir = data_dir+'/'+ folder_dir+'/'+patent_folder+'/'+patent_folder+'.XML'

		print("Reading folder ", folder_dirs.index(folder_dir)+1, " out of ", len(folder_dirs), 
		" , Patent ", os.listdir(data_dir+'/'+folder_dir).index(patent_folder)+1, " out of ",  len(os.listdir(data_dir+'/'+folder_dir)) )
	
		patent_n += 1
		patent_title, patent_publ_number, patent_ipc, patent_applicants, patent_date, patent_claims = \
		parse_patent(patent_dir)

		if patent_title and len(patent_claims) > 0:
			add_patent_to_db(patent_title, patent_publ_number, patent_ipc, patent_applicants, \
				patent_date, patent_claims)

print("Done ....")
print("Successfully loaded ", patent_n, ' patents ...')
			
	