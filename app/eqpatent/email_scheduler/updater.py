
'''
from apscheduler.schedulers.background import BackgroundScheduler

from dateutil.relativedelta import relativedelta
from datetime import datetime
import time
import pytz

from django.core.mail import send_mail
from django.conf import settings

from landing_page.models import User
from landing_page.email_utils import send_first_week_email, send_second_week_email

def send_weekly_emails():

	users = User.objects.all()
	for user in users:

		if not user.is_staff:
			user_created_time = user.created_time
			now = datetime.now()
			now = pytz.utc.localize(now)
			diff = relativedelta(now, user_created_time).days

			if diff == 7:
				if not user.first_email_sent:
					
					send_first_week_email(user.email, user.first_name)
					user.first_email_sent = True
					user.save()

			if diff == 14:
				if not user.second_email_sent:
					send_second_week_email(user.email, user.first_name)
					user.second_email_sent = True

					user = user.save()

				if user.active:
					user.active = False
					user = user.save()

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(send_weekly_emails, 'interval', hours=12)
    scheduler.start()
'''