# Generated by Django 2.1.5 on 2019-07-22 09:20

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('landing_page', '0006_user_created_at'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='created_at',
            new_name='created_time',
        ),
    ]
