from django.conf.urls import url

from . import views

urlpatterns = [
    
    url(r'^$', views.landing_page_index, name='landing_page_index'),

    #url(r'^registration/$', views.registration, name='registration'),

    url(r'^impressum/$', views.impressum, name='impressum'),
    url(r'^datenschutz/$', views.datenschutz, name='datenschutz'),
]