
from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.core.mail import send_mail
from django.conf import settings

from .models import Email
from .forms import UserForm
from .email_utils import send_registration_info_email, send_welcome_email

# Create your views here.
def landing_page_index(request):
	
	if request.method == 'POST':

		new_email = Email(text=request.POST.get('email'))
		new_email.save()

		#send_registration_info_email(request.POST.get('email'))

		return render(request, 'landing_page_index.html', {'success':True})	
	else:
		return render(request, 'landing_page_index.html', {})

def registration(request):
	
	if request.method == 'POST':
		user_form = UserForm(request.POST)
			
		if user_form.is_valid():
			
			user = user_form.save()
			
			raw_password = user_form.cleaned_data.get('password1')

			user = authenticate(email=user.email, password=raw_password)
			login(request, user)

			send_welcome_email(user.email, user.first_name)

			if Email.objects.filter(text=user.email).count() == 0:
				new_email = Email(text=user.email)
				new_email.save()

			return redirect(landing_page_index)
		else:
			return render(request, 'registration.html', {'user_form': user_form})
	else:
		user_form = UserForm()
		return render(request, 'registration.html', {'user_form': user_form})

def impressum(request):
	return render(request, 'impressum.html', {})

def datenschutz(request):
	return render(request, 'datenschutz.html', {})
	