from django.apps import AppConfig


class LandingPageConfig(AppConfig):
    name = 'landing_page'

    #def ready(self):
    #    from email_scheduler import updater
    #    updater.start()
