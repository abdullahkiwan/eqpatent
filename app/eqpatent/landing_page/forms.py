
from django.contrib.auth.forms import UserCreationForm
from landing_page.models import User
from django import forms

class UserForm(UserCreationForm):

	error_css_class = "error"
	
	def __init__(self, *args, **kwargs):
		super(UserCreationForm, self).__init__(*args, **kwargs)
		
		self.fields['password1'].required = True
		self.fields['password2'].required = True

		for field in self.fields:
			self.fields[field].help_text = None

	class Meta:
		model = User
		fields = ('email', 'first_name', 'last_name', 'company_name', 'company_type')

	def clean_email(self):
		email = self.cleaned_data.get('email')

		if email and User.objects.filter(email=email).count():
		    raise forms.ValidationError("This email address is already in use. Please supply a different email address.")
		return email

	def clean_password2(self):
		password1 = self.cleaned_data.get("password1", "")
		password2 = self.cleaned_data.get("password2", "")

		if password1 and password2:  # If both passwords has value
			if password1 != password2:
				raise forms.ValidationError(("Passwords didn't match."))
			return password2