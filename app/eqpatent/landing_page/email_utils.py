
from django.core.mail import send_mail
from django.conf import settings


def send_registration_info_email(email_address):

	send_mail(
		'EQPatent Registration',
		'Registration here : http://52.59.254.102:8000/registration/',
		settings.EMAIL_HOST_USER,
		[email_address],
		fail_silently=False,
	)

def send_welcome_email(email_address, first_name):

	email_subject = "Willkommens"
	
	email_text = "Hallo " + first_name + "\n\n"
	email_text += "Willkommen an Board. Du gehörst zu unseren Unterstützern der ersten Stunde und \
	Dir gebührt damit das Privileg uns Feedback zu geben bis es kracht.\n\n\
	Mit EQPatent wollen wir Patentsuchen revolutionieren. Probiere es direkt einmal aus: \
	Nutze die umfangreiche integrierte Synonymdatenbank und unterscheide in Deiner Suche \
	zwischen unabhängigen und abhängigen Ansprüchen.\n\n\
	Falls dies nicht geklappt hat, dann sende kurz ein ‚help‘ an hello@eqpatent.de und \
	wir melden uns sofort bei dir."
	email_text += "Beste Grüße aus Bonn,\nAbdullah, Mark und Silvan"

	send_mail(email_subject, email_text, settings.EMAIL_HOST_USER, [email_address], fail_silently=False)

def send_first_week_email(email_address, first_name):

	email_subject = "FIRST EMAIL SUBJECT"
	
	email_text = "Hallo " + first_name + "\n\n"
	email_text += "Jetzt sind schon 7 Tage seit deiner Registrierung vergangen. "
	email_text += "Zeit für ein Résumé. Klicke einfach Reply und gib uns dein Feedback gerade aus. "
	email_text += "Was stört Dich? Was findest Du gut? \n\n" 
	email_text += "Melde Dich doch einfach schnell!\n\n"
	email_text += "Beste Grüße aus Bonn,\nAbdullah, Mark und Silvan"

	send_mail(email_subject, email_text, settings.EMAIL_HOST_USER, [email_address], fail_silently=False)

def send_second_week_email(email_address, first_name):

	email_subject = "SECOND EMAIL SUBJECT"
	
	email_text = "Hallo " + first_name + "\n\n"
	email_text += "Ein Textbaustein ist ein Textfragment, das wiederkehrende Verwendung findet. \
	Der Begriff wird sowohl in der Textverarbeitung als auch in der Sprachwissenschaft mit abweichenden \
	Bedeutungen verwendet. In der Textverarbeitung ist ein Textbaustein ein vorgefertigter Text, der in \
	einen anderen Text eingefügt wird.\n\n"
	
	email_text += "Beste Grüße aus Bonn,\nAbdullah, Mark und Silvan"

	send_mail(email_subject, email_text, settings.EMAIL_HOST_USER, [email_address], fail_silently=False)
