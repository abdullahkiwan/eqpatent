/*

	jQuery Tags Input Plugin 1.3.3

	Copyright (c) 2011 XOXCO, Inc

	Documentation for this plugin lives here:
	http://xoxco.com/clickable/jquery-tags-input

	Licensed under the MIT license:
	http://www.opensource.org/licenses/mit-license.php

	ben@xoxco.com

*/

var synonyms;
var success_ajax = false;

var saved_synonyms = {};
var n_synonyms = {};

(function($) {

	var delimiter = new Array();
	var tags_callbacks = new Array();
	$.fn.doAutosize = function(o){
	    var minWidth = $(this).data('minwidth'),
	        maxWidth = $(this).data('maxwidth'),
	        val = '',
	        input = $(this),
	        testSubject = $('#'+$(this).data('tester_id'));

	    if (val === (val = input.val())) {return;}

	    // Enter new content into testSubject
	    var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	    testSubject.html(escaped);
	    // Calculate new width + whether to change
	    var testerWidth = testSubject.width(),
	        newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
	        currentWidth = input.width(),
	        isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
	                             || (newWidth > minWidth && newWidth < maxWidth);

	    // Animate width
	    if (isValidWidthChange) {
	        input.width(newWidth);
	    }
  	};

	$.fn.resetAutosize = function(options){
		// alert(JSON.stringify(options));
		var minWidth =  $(this).data('minwidth') || options.minInputWidth || $(this).width(),
		    maxWidth = $(this).data('maxwidth') || options.maxInputWidth || ($(this).closest('.tagsinput').width() - options.inputPadding),
		    val = '',
		    input = $(this),
		    testSubject = $('<tester/>').css({
		        position: 'absolute',
		        top: -9999,
		        left: -9999,
		        width: 'auto',
		        fontSize: input.css('fontSize'),
		        fontFamily: input.css('fontFamily'),
		        fontWeight: input.css('fontWeight'),
		        letterSpacing: input.css('letterSpacing'),
		        whiteSpace: 'nowrap'
		    }),
		    testerId = $(this).attr('id')+'_autosize_tester';
		if(! $('#'+testerId).length > 0){
		  testSubject.attr('id', testerId);
		  testSubject.appendTo('body');
		}

		input.data('minwidth', minWidth);
		input.data('maxwidth', maxWidth);
		input.data('tester_id', testerId);
		input.css('width', minWidth);
	};

	$.fn.addTag = function(value,options) {

			options = jQuery.extend({focus:false,callback:true},options);
			this.each(function() {
				
				var id = $(this).attr('id');

				var tagslist = $(this).val().split(delimiter[id]);
				if (tagslist[0] == '') {
					tagslist = new Array();
				}

				value = jQuery.trim(value);

				if (options.unique) {
					var skipTag = $(this).tagExist(value);
					if(skipTag == true) {
					    //Marks fake input as not_valid to let styling it
    				    $('#'+id+'_tag').addClass('not_valid');
    				}
				} else {
					var skipTag = false;
				}

				
				if (value !='' && skipTag != true) {

                    var new_span = $('<span>').addClass('tag');
                    new_span.append(

                    	$('<a><img id="delete_btn" src="/static/img/delete.jpg" title="Delete word">', {
                            href  : '#',
                            title : 'Remove Keyword',
                            text  : 'x '
                        }).click(function () {
                            
							return $('#' + id).removeTag(escape(value));
							
                        }).css({fontSize: 15}),

                        $('<span>').text(value).append('')

                    ).insertBefore('#' + id + '_addTag');

                    if(!id.includes("synonyms") && !id.includes("general_types") && !id.includes("special_types"))
					{
						new_span.append(

	                    	$('<a><img id="synonyms_btn" src="/static/img/synonyms.jpg" title="Synonyms : 0"></a>',	                           
	                        ).click(function () {
	                           
	                        	if ( value.split(" ").length > 1)
									value = value.split(" ").join("_");

	                        	modal_window = document.getElementById('modal_'+id+'_info_'+value);
								modal_window.style.display = "block";
								
	                        }).css({fontSize: 15}),

	                    );
					}

					if(!id.includes("synonyms") && !id.includes("general_types") && !id.includes("special_types"))
					{
						var and_span = $('<span>').addClass('and_tag').attr('id', id+'_'+value+'_and');
                    	and_span.append(

                        	$('<span>').text("AND").append('')

                    	).insertBefore('#' + id + '_addTag');
                    }

					tagslist.push(value);

					$('#'+id+'_tag').val('');
					if (options.focus) {
						$('#'+id+'_tag').focus();
					} else {
						$('#'+id+'_tag').blur();
					}

					$.fn.tagsInput.updateTagsField(this,tagslist);

					if (options.callback && tags_callbacks[id] && tags_callbacks[id]['onAddTag']) {
						var f = tags_callbacks[id]['onAddTag'];
						f.call(this, value);
					}
					if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
					{
						var i = tagslist.length;
						var f = tags_callbacks[id]['onChange'];
						f.call(this, $(this), tagslist[i-1]);
					}
				}

			});

			if(! $(this).attr('id').includes("synonyms") && !$(this).attr('id').includes("general_types") && !$(this).attr('id').includes("special_types"))
				fetch_synonyms(value, $(this).attr('id'));
			
			return false;
		};

	$.fn.removeTag = function(value) {
			value = unescape(value);
			this.each(function() {
				var id = $(this).attr('id');

				var old = $(this).val().split(delimiter[id]);

				$('#'+id+'_tagsinput .and_tag').remove();

				$('#'+id+'_tagsinput .tag').remove();
				str = '';
				for (i=0; i< old.length; i++) {
					if (old[i]!=value) {
						str = str + delimiter[id] +old[i];
					}
				}

				$.fn.tagsInput.importTags(this,str);

				if (tags_callbacks[id] && tags_callbacks[id]['onRemoveTag']) {
					var f = tags_callbacks[id]['onRemoveTag'];
					f.call(this, value);
				}
			});

			return false;
		};

	$.fn.tagExist = function(val) {
		var id = $(this).attr('id');
		var tagslist = $(this).val().split(delimiter[id]);
		return (jQuery.inArray(val, tagslist) >= 0); //true when tag exists, false when not
	};

   // clear all existing tags and import new ones from a string
   $.fn.importTags = function(str) {
      var id = $(this).attr('id');
      $('#'+id+'_tagsinput .tag').remove();
      $.fn.tagsInput.importTags(this,str);
   }

	$.fn.tagsInput = function(options) {

    var settings = jQuery.extend({
      interactive:true,
      defaultText:'',
      minChars:0,
      width:'300px',
      height:'100px',
      autocomplete: {selectFirst: false },
      hide:true,
      delimiter: ',',
      unique:true,
      removeWithBackspace:true,
      placeholderColor:'#666666',
      autosize: true,
      comfortZone: 20,
      inputPadding: 6*2
    },options);

    	var uniqueIdCounter = 0;

		this.each(function() {
         // If we have already initialized the field, do not do it again
         if (typeof $(this).attr('data-tagsinput-init') !== 'undefined') {
            return;
         }

         // Mark the field as having been initialized
         $(this).attr('data-tagsinput-init', true);

			if (settings.hide) {
				$(this).hide();
			}
			var id = $(this).attr('id');
			if (!id || delimiter[$(this).attr('id')]) {
				id = $(this).attr('id', 'tags' + new Date().getTime() + (uniqueIdCounter++)).attr('id');
			}

			var data = jQuery.extend({
				pid:id,
				real_input: '#'+id,
				holder: '#'+id+'_tagsinput',
				input_wrapper: '#'+id+'_addTag',
				fake_input: '#'+id+'_tag'
			},settings);

			delimiter[id] = data.delimiter;

			if (settings.onAddTag || settings.onRemoveTag || settings.onChange) {
				tags_callbacks[id] = new Array();
				tags_callbacks[id]['onAddTag'] = settings.onAddTag;
				tags_callbacks[id]['onRemoveTag'] = settings.onRemoveTag;
				tags_callbacks[id]['onChange'] = settings.onChange;
			}

			var markup = '<div id="'+id+'_tagsinput" class="tagsinput"><div id="'+id+'_addTag">';

			if (settings.interactive) {
				markup = markup + '<input id="'+id+'_tag" value="" data-default="'+settings.defaultText+'" />';
			}

			markup = markup + '</div><div class="tags_clear"></div></div>';

			$(markup).insertAfter(this);

			$(data.holder).css('width',settings.width);
			$(data.holder).css('min-height',settings.height);
			$(data.holder).css('height',settings.height);

			if ($(data.real_input).val()!='') {
				$.fn.tagsInput.importTags($(data.real_input),$(data.real_input).val());
			}
			if (settings.interactive) {
				$(data.fake_input).val($(data.fake_input).attr('data-default'));
				$(data.fake_input).css('color',settings.placeholderColor);
		        $(data.fake_input).resetAutosize(settings);

				$(data.holder).bind('click',data,function(event) {
					$(event.data.fake_input).focus();
				});

				$(data.fake_input).bind('focus',data,function(event) {
					if ($(event.data.fake_input).val()==$(event.data.fake_input).attr('data-default')) {
						$(event.data.fake_input).val('');
					}
					$(event.data.fake_input).css('color','#000000');
				});

				if (settings.autocomplete_url != undefined) {
					autocomplete_options = {source: settings.autocomplete_url};
					for (attrname in settings.autocomplete) {
						autocomplete_options[attrname] = settings.autocomplete[attrname];
					}

					if (jQuery.Autocompleter !== undefined) {
						$(data.fake_input).autocomplete(settings.autocomplete_url, settings.autocomplete);
						$(data.fake_input).bind('result',data,function(event,data,formatted) {
							if (data) {
								$('#'+id).addTag(data[0] + "",{focus:true,unique:(settings.unique)});
							}
					  	});
					} else if (jQuery.ui.autocomplete !== undefined) {
						$(data.fake_input).autocomplete(autocomplete_options);
						$(data.fake_input).bind('autocompleteselect',data,function(event,ui) {
							$(event.data.real_input).addTag(ui.item.value,{focus:true,unique:(settings.unique)});
							return false;
						});
					}


				} else {
						// if a user tabs out of the field, create a new tag
						// this is only available if autocomplete is not used.
						$(data.fake_input).bind('blur',data,function(event) {
							var d = $(this).attr('data-default');
							if ($(event.data.fake_input).val()!='' && $(event.data.fake_input).val()!=d) {
								if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
									$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
							} else {
								$(event.data.fake_input).val($(event.data.fake_input).attr('data-default'));
								$(event.data.fake_input).css('color',settings.placeholderColor);
							}
							return false;
						});

				}
				// if user types a default delimiter like comma,semicolon and then create a new tag
				$(data.fake_input).bind('keypress',data,function(event) {
					if (_checkDelimiter(event)) {
					    event.preventDefault();
						if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
							$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
					  	$(event.data.fake_input).resetAutosize(settings);
						return false;
					} else if (event.data.autosize) {
			            $(event.data.fake_input).doAutosize(settings);

          			}
				});
				//Delete last tag on backspace
				data.removeWithBackspace && $(data.fake_input).bind('keydown', function(event)
				{
					if(event.keyCode == 8 && $(this).val() == '')
					{
						 event.preventDefault();
						 var last_tag = $(this).closest('.tagsinput').find('.tag:last').text();
						 var id = $(this).attr('id').replace(/_tag$/, '');
						 last_tag = last_tag.replace(/[\s]+x$/, '');
						 $('#' + id).removeTag(escape(last_tag));
						 $(this).trigger('focus');
					}
				});
				$(data.fake_input).blur();

				//Removes the not_valid class when user changes the value of the fake input
				if(data.unique) {
				    $(data.fake_input).keydown(function(event){
				        if(event.keyCode == 8 || String.fromCharCode(event.which).match(/\w+|[áéíóúÁÉÍÓÚñÑ,/]+/)) {
				            $(this).removeClass('not_valid');
				        }
				    });
				}
			} // if settings.interactive
		});

		return this;

	};

	$.fn.tagsInput.updateTagsField = function(obj,tagslist) {
		var id = $(obj).attr('id');
		$(obj).val(tagslist.join(delimiter[id]));
	};

	$.fn.tagsInput.importTags = function(obj,val) {
		$(obj).val('');
		var id = $(obj).attr('id');
		var tags = val.split(delimiter[id]);
		for (i=0; i<tags.length; i++) {
			$(obj).addTag(tags[i],{focus:false,callback:false});
		}
		if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
		{
			var f = tags_callbacks[id]['onChange'];
			f.call(obj, obj, tags[i]);
		}
	};

   /**
     * check delimiter Array
     * @param event
     * @returns {boolean}
     * @private
     */
   var _checkDelimiter = function(event){
      var found = false;
      if (event.which == 13) {
         return true;
      }

      if (typeof event.data.delimiter === 'string') {
         if (event.which == event.data.delimiter.charCodeAt(0)) {
            found = true;
         }
      } else {
         $.each(event.data.delimiter, function(index, delimiter) {
            if (event.which == delimiter.charCodeAt(0)) {
               found = true;
            }
         });
      }

      return found;
   }
})(jQuery);

function cal_synonyms(id, keyword) {

	modal_id = id+'_info_'+keyword;

	var checked = $("#modal_"+modal_id+" :checkbox:checked").length;
	var modal = document.getElementById("modal_"+modal_id);
	var additional_synonyms = modal.getElementsByClassName('tag').length;
	
	var n = checked + additional_synonyms;

	var btn = document.getElementById('synonyms_btn');
	btn.setAttribute('Title', "Synonyms : "+n);
}

function check(id) {	
	
	id.checked = id.checked;
}


function fetch_synonyms(keyword, id) {
    
    console.log("Fetching synonyms is working!") // sanity check
    $.ajax({
        url : "find_synonyms/", // the endpoint
        type : "POST", // http method
        data : { 'keyword' : keyword }, // data sent with the post request
        dataType: 'json',
        // handle a successful response
        success : function(data) {
        	
        	synonyms = data.synonyms;
			general_types = data.general_types
			special_types = data.special_types
			create_synonyms_window(id, keyword, synonyms, general_types, special_types);
        }
    }); 
}

function save_synonyms(id, keyword)
{
	var modal_id = 'modal_'+id+'_info_'+keyword;

	var checked = [];
	$('#'+modal_id+' :checkbox:checked').each(function(i)
	{
    	checked[i] = $(this).val();
    });

    saved_synonyms[modal_id]['checked'] = checked;
	saved_synonyms[modal_id]['additional_synonyms'] = $('#'+id+"_additional_synonyms_"+keyword).val();
	saved_synonyms[modal_id]['additional_general_types'] = $('#'+id+"_additional_general_types_"+keyword).val();
	saved_synonyms[modal_id]['additional_special_types'] = $('#'+id+"_additional_special_types_"+keyword).val();
}

function retrieve_synonyms(id, keyword)
{
	var modal_id = 'modal_'+id+'_info_'+keyword;
	var checks = $('#'+modal_id+' :checkbox');
	checks.removeAttr('checked');
	
	var inputs = document.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++) 
		if (inputs[i].type == "checkbox")
			if (saved_synonyms[modal_id]['checked'].includes(inputs[i].value))
	    		inputs[i].checked = true;

	$('#'+id+"_additional_synonyms_"+keyword).importTags(saved_synonyms[modal_id]['additional_synonyms']);
	$('#'+id+"_additional_general_types_"+keyword).importTags(saved_synonyms[modal_id]['additional_general_types']);
	$('#'+id+"_additional_special_types_"+keyword).importTags(saved_synonyms[modal_id]['additional_special_types']);
}

function close_synonyms_window(id, keyword, modal_window)
{
	modal_id = id+'_info_'+keyword;
	modal_close = document.getElementById('close_'+modal_id);
	
	modal_close.onclick = function() 
	{
		retrieve_synonyms(id, keyword);
		
		modal_window.style.display = "none";
	}
}

function save_synonyms_window(id, keyword)
{
	
	modal_id = id+'_info_'+keyword;
	modal_save = document.getElementById('save_'+modal_id);
	
	modal_save.onclick = function() {

		save_synonyms(id, keyword);
		cal_synonyms(id, keyword);

		modal_window = document.getElementById('modal_'+modal_id);
		modal_window.style.display = "none";
	}
}

function create_synonyms_window(id, keyword, synonyms, type_of, has_types) 
{
	keyword_with_underscore = keyword
	if ( keyword.split(" ").length > 1)
		keyword_with_underscore = keyword.split(" ").join("_");

	if ( ! $('#'+id+"_info_"+keyword_with_underscore).length )
	{
		var modal_div = '';

		modal_div += '<div id="modal_';
		modal_div += id;
		modal_div += '_info_';
		modal_div += keyword_with_underscore;
		modal_div += '" class="overflow-auto modal">';
		modal_div += '<div class="modal-content">';

		modal_div += '<div class="form-inline">';
		modal_div += '<span id="close_';
		modal_div += id;
		modal_div += '_info_';
		modal_div += keyword_with_underscore;
		modal_div += '" class="close" style="color: #FF0000;">&times;</span>';			  		
		
		modal_div += '<span style="margin-right:20px; color:#32CD32;" id="save_';
		modal_div += id;
		modal_div += '_info_';
		modal_div += keyword_with_underscore;
		modal_div += '" class="close">&#10003;</span>';
		modal_div += '</div>';

		modal_div += '<div class="container">';
		modal_div += '<h2>';
		modal_div += keyword; 
		modal_div += '</h2>';
  		modal_div += '<div class="row">';

  		modal_div += '<div class="col-sm-4">';
    	modal_div += '<h4>Hyperonyme</h4>'
    	if (general_types.length > 0)
			for (idx in general_types)
			{
				modal_div += '<div class="checkbox">';
				modal_div += '<label><input type="checkbox" name="';
				modal_div += id;
				modal_div += '_general_types_';
				modal_div += keyword_with_underscore;
				modal_div += '"';
				modal_div += 'id = "general_types_';
				modal_div += idx;
				modal_div += '"';
				modal_div += 'onclick="check(general_types_'
				modal_div += idx;
				modal_div += ')"';
				modal_div += 'value="';
				modal_div += general_types[idx];
				modal_div += '">';
				modal_div += general_types[idx];
				modal_div += '</label></div>';
			} 
		else
			modal_div += '<p>No results</p>';
		modal_div += '</div>';

    	modal_div += '<div class="col-sm-4">';
    	modal_div += '<h4>Synonyme</h4>';
		if (synonyms.length > 0) 
			for (idx in synonyms)
			{
				modal_div += '<div class="checkbox">';
				modal_div += '<label><input type="checkbox" name="';
				modal_div += id;
				modal_div += '_synonyms_';
				modal_div += keyword_with_underscore;
				modal_div += '"';
				modal_div += 'id = "synonyms_';
				modal_div += idx;
				modal_div += '"';
				modal_div += 'onclick="check(synonyms_'
				modal_div += idx;
				modal_div += ')"';
				modal_div += 'value="';
				modal_div += synonyms[idx];
				modal_div += '">';
				modal_div += synonyms[idx];
				modal_div += '</label></div>';	
			}
		else
			modal_div += '<p>No results</p>';
		modal_div += '</div>';
    	
		modal_div += '<div class="col-sm-4">';
		modal_div += '<h4>Hyponym</h4>'
		if (special_types.length > 0)
			for (idx in special_types)
			{
				modal_div += '<div class="checkbox">';
				modal_div += '<label><input type="checkbox" name="';
				modal_div += id;
				modal_div += '_special_types_';
				modal_div += keyword_with_underscore;
				modal_div += '"';
				modal_div += 'id = "special_types_';
				modal_div += idx;
				modal_div += '"';
				modal_div += 'onclick="check(special_types_'
				modal_div += idx;
				modal_div += ')"';
				modal_div += 'value="';
				modal_div += special_types[idx];
				modal_div += '">';
				modal_div += special_types[idx];
				modal_div += '</label></div>';
			}
		else
			modal_div += '<p>No results</p>';
		modal_div += '</div>';

		modal_div += '</div></div>';

		modal_div += '<br>';

		modal_div += '<div class="form-group">'
		modal_div += '<label class="inline-label" style="margin-right:57px; margin-left:40px;">Synonym hinzufügen</label>'
		modal_div += '<input style="display: block;" type="text" class="input" name="';
		modal_div += id;
		modal_div += '_additional_synonyms_';
		modal_div += keyword_with_underscore; 
		modal_div += '" id="';
		modal_div += id;
		modal_div += '_additional_synonyms_';
		modal_div += keyword_with_underscore;
		modal_div += '" value="">';
		modal_div += '</div>';

		modal_div += '<div class="form-group">'
		modal_div += '<label class="inline-label" style="margin-right:40px; margin-left:40px;">Hyperonym hinzufügen</label>'
		modal_div += '<input style="display: block; margin-left:40px;" type="text" class="input" name="';
		modal_div += id;
		modal_div += '_additional_general_types_';
		modal_div += keyword_with_underscore; 
		modal_div += '" id="';
		modal_div += id;
		modal_div += '_additional_general_types_';
		modal_div += keyword_with_underscore;
		modal_div += '" value="">';
		modal_div += '</div>';

		modal_div += '<div class="form-group">'
		modal_div += '<label class="inline-label" style="margin-right:55px; margin-left:40px;">Hyponym hinzufügen</label>'
		modal_div += '<input style="display: block;" type="text" class="input" name="';
		modal_div += id;
		modal_div += '_additional_special_types_';
		modal_div += keyword_with_underscore; 
		modal_div += '" id="';
		modal_div += id;
		modal_div += '_additional_special_types_';
		modal_div += keyword_with_underscore;
		modal_div += '" value="">';
		modal_div += '</div>';

		modal_div += '</div></div>';

		$("#deep_search_form").append(modal_div);

		$('#'+id+"_additional_synonyms_"+keyword_with_underscore).tagsInput({width:'1000px', height:'60px', placeholder:''});
		$('#'+id+"_additional_general_types_"+keyword_with_underscore).tagsInput({width:'1000px', height:'60px', placeholder:''});
		$('#'+id+"_additional_special_types_"+keyword_with_underscore).tagsInput({width:'1000px', height:'60px', placeholder:''});
		
		modal_id = id+'_info_'+keyword_with_underscore;
		modal_window = document.getElementById('modal_'+modal_id);

		saved_synonyms['modal_'+modal_id] = {}
		saved_synonyms['modal_'+modal_id]['checked'] = [];
		saved_synonyms['modal_'+modal_id]['additional_synonyms'] = '';
		saved_synonyms['modal_'+modal_id]['additional_general_types'] = '';
		saved_synonyms['modal_'+modal_id]['additional_special_types'] = '';

		modal_close = document.getElementById('close_'+modal_id);
		modal_save = document.getElementById('save_'+modal_id);

		save_synonyms_window(id, keyword_with_underscore);
		close_synonyms_window(id, keyword_with_underscore, modal_window);
	}
}


