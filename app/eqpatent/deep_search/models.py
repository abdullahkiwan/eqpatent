
from django.contrib.auth.models import AbstractUser, AbstractBaseUser, \
    BaseUserManager, PermissionsMixin
from django.db import models
from elasticsearch_dsl import DocType, Text, Date
from datetime import datetime 
 
from landing_page.models import User

#def get_username(self):
#    return self.username

#User.add_to_class("__str__", get_username)

class UserTracking(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    added_time = models.DateTimeField(default=datetime.now, blank=True, null=True)

    def __str__(self):
        return self.user.email

class Word(models.Model):
    text = models.CharField(max_length=20)

    def __str__(self):
        return self.text

class SynonymPair(models.Model):
    word_1 =  models.CharField(default="", max_length=30)
    word_2 = models.CharField(default="", max_length=30)
    keywords = models.CharField(default="", max_length=1000)
    added_time = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.word_1 + ' & ' + self.word_2

class GeneralTypePair(models.Model):
    word_1 =  models.CharField(default="", max_length=30)
    word_2 = models.CharField(default="", max_length=30)
    keywords = models.CharField(default="", max_length=1000)
    added_time = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.word_1 + ' & ' + self.word_2

class SpecialTypePair(models.Model):
    word_1 =  models.CharField(default="", max_length=30)
    word_2 = models.CharField(default="", max_length=30)
    keywords = models.CharField(default="", max_length=1000)
    added_time = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.word_1 + ' & ' + self.word_2
        
class Claim(models.Model):

    text = models.CharField(max_length=1000)
    patent = models.ForeignKey('Patent', on_delete=models.CASCADE, blank=True, null=True)
    is_independant = models.BooleanField(default=True)
    
    def ind_claim_indexing(self):
        obj = IndependantClaimIndex(
            meta={'id': self.id},
            text=self.text,
            patent=self.patent.id,
            is_independant=self.is_independant
        )
        obj.save()
        return obj.to_dict(include_meta=True)

    def dep_claim_indexing(self):
        obj = DependantClaimIndex(
            meta={'id': self.id},
            text=self.text,
            patent=self.patent.id,
            is_independant=self.is_independant
        )
        obj.save()
        return obj.to_dict(include_meta=True)

    def __str__(self):
        return self.text
    

class Patent(models.Model):
    list_per_page = 500
    title = models.CharField(max_length=100)
    publication_number = models.CharField(max_length=20)
    ipc = models.CharField(max_length=10, blank=True, null=True)
    applicants = models.CharField(max_length=30, blank=True, null=True)
    date = models.CharField(max_length=10, blank=True, null=True)
    
    def __str__(self):
        return self.publication_number

class IndependantClaimIndex(DocType):
    text = Text()
    patent = Text()
    is_independant = Text()

    class Meta:
        index = 'independant-claim'

class DependantClaimIndex(DocType):
    text = Text()
    patent = Text()
    is_independant = Text()
    
    class Meta:
        index = 'dependant-claim'