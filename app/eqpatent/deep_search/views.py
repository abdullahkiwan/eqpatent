from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.forms import UserCreationForm

from textblob import TextBlob

from collections import OrderedDict
from fpdf import FPDF

import datetime
import json

from .models import Claim, Word, Patent, UserTracking

from .nlp_utils import *
from .db_utils import *
from .search_utils import *

from landing_page.views import landing_page_index

from .spacy_synonyms import get_spacy_synonyms
from .system_utils import get_download_folder

def deep_search_index(request):

	context = {}
	if request.GET.get('name') and request.GET.get('name') == 'back':
		context['retrieve_user_input'] = True
		if 'ipc_class' in request.session:
			context['ipc_class'] = request.session['ipc_class']
	else:
		if 'independant_mandatory' in request.session:

			if 'ipc_class' in request.session:
				del request.session['ipc_class']
			del request.session['independant_mandatory']
			del request.session['independant_optional']
			del request.session['dependant_mandatory']
			del request.session['dependant_optional']

	return render(request, 'index.html', context)

def search_view(request):

	independant_mandatory_keywords = get_keywords_from_request(request, 'independant_mandatory')
	independant_optional_keywords = get_keywords_from_request(request, 'independant_optional')
	dependant_mandatory_keywords = get_keywords_from_request(request, 'dependant_mandatory')
	dependant_optional_keywords = get_keywords_from_request(request, 'dependant_optional')

	context = search_patents(request, independant_mandatory_keywords,\
									independant_optional_keywords,\
									dependant_mandatory_keywords,\
									dependant_optional_keywords)

	return render(request, 'search_results.html',context)
	
def logout_user(request):
	
	if request.user.is_authenticated:
		logout(request)
	return redirect(landing_page_index)

def get_synonyms_from_request(request, keywords_list, keywords_type):

	if not keywords_list: return None

	synonyms_list = []
	if keywords_list:
		for idx in range(len(keywords_list)):
			synonyms = {}
			for word in keywords_list[idx]:

				if word:

					tmp_word = word
					if len(word.split()) > 1: 
						tmp_word = '_'.join(word.split())

					word = word.lower()
					word = word.replace('_', ' ')
					synonyms[word] = {}
					synonyms[word]['synonyms'] = []
					synonyms[word]['additional_synonyms'] = []

					synonyms[word]['synonyms'] = request.POST.getlist(keywords_type+'_'+str(idx+1)+'_synonyms_'+tmp_word)
					
					additional_synonyms = request.POST.get(keywords_type+'_'+str(idx+1)+'_additional_synonyms_'+tmp_word).split(',')
					if additional_synonyms[0]:
						additional_synonyms = preprocess_word_list(additional_synonyms)
						add_db_synonyms(word, additional_synonyms, ','.join(keywords_list[idx]))
						synonyms[word]['additional_synonyms'] = additional_synonyms

				elif word:
					synonyms[word.lower()] = None

			synonyms_list.append(synonyms)
	return synonyms_list                                    

def get_keywords_from_request(request, keywords_type):

	keywords_list = []
	for key in request.POST:
		if key.startswith(keywords_type) and len(key.split('_')) == 3:
			keywords_list.append(request.POST[key].split(','))

	return keywords_list

def search_patents(request, independant_mandatory_keywords,\
									independant_optional_keywords,\
									dependant_mandatory_keywords,\
									dependant_optional_keywords):
	
	ipc_class = request.POST.get('ipc_class')
	
	# Create a dictionary consisting of keywords as the dictionary keys, and synonym lists as dictionary values.
	independant_mandatory_keywords_with_synonyms = get_synonyms_from_request(request, independant_mandatory_keywords, 'independant_mandatory')
	independant_optional_keywords_with_synonyms = get_synonyms_from_request(request, independant_optional_keywords, 'independant_optional')
	dependant_mandatory_keywords_with_synonyms = get_synonyms_from_request(request, dependant_mandatory_keywords, 'dependant_mandatory')
	dependant_optional_keywords_with_synonyms = get_synonyms_from_request(request, dependant_optional_keywords, 'dependant_optional')

	# Get a dict of keywords including a list synonyms
	independant_mandatory_keywords = add_synonyms_to_keyword_list(independant_mandatory_keywords_with_synonyms)
	independant_optional_keywords = add_synonyms_to_keyword_list(independant_optional_keywords_with_synonyms)
	dependant_mandatory_keywords = add_synonyms_to_keyword_list(dependant_mandatory_keywords_with_synonyms)
	dependant_optional_keywords = add_synonyms_to_keyword_list(dependant_optional_keywords_with_synonyms)

	if ipc_class:
		request.session['ipc_class'] = ipc_class
	request.session['independant_mandatory'] = independant_mandatory_keywords_with_synonyms
	request.session['independant_optional'] = independant_optional_keywords_with_synonyms
	request.session['dependant_mandatory'] = dependant_mandatory_keywords_with_synonyms
	request.session['dependant_optional'] = dependant_optional_keywords_with_synonyms

	if not independant_mandatory_keywords_with_synonyms[0]: 
		independant_mandatory_keywords=None
		independant_mandatory_keywords_with_synonyms=None
	if not independant_optional_keywords_with_synonyms[0]:
		independant_optional_keywords=None
		independant_optional_keywords_with_synonyms=None
	if not dependant_mandatory_keywords_with_synonyms[0]:
		dependant_mandatory_keywords=None
		dependant_mandatory_keywords_with_synonyms=None
	if not dependant_optional_keywords_with_synonyms[0]:
		dependant_optional_keywords=None 
		dependant_optional_keywords_with_synonyms=None

	independant_mandatory_query = prepare_query(independant_mandatory_keywords_with_synonyms)
	independant_mandatory_optional_query = prepare_query(independant_mandatory_keywords_with_synonyms, \
														independant_optional_keywords_with_synonyms)
	dependant_mandatory_query = prepare_query(dependant_mandatory_keywords_with_synonyms)
	dependant_mandatory_optional_query = prepare_query(dependant_mandatory_keywords_with_synonyms, \
														dependant_optional_keywords_with_synonyms)

	similar_patents = []
	similar_patent_ids = []

	independant_mandatory_response = search(independant_mandatory_query, is_indep=True)
	independant_mandatory_patent_ids = get_similar_patent_ids(independant_mandatory_response)

	dependant_mandatory_response = search(dependant_mandatory_query, is_indep=False)
	dependant_mandatory_patent_ids = get_similar_patent_ids(dependant_mandatory_response)

	independant_mandatory_optional_response = search(independant_mandatory_optional_query, is_indep=True)
	independant_mandatory_optional_patent_ids = get_similar_patent_ids(independant_mandatory_optional_response)

	dependant_mandatory_optional_response = search(dependant_mandatory_optional_query, is_indep=False)
	dependant_mandatory_optional_patent_ids = get_similar_patent_ids(dependant_mandatory_optional_response)
	
	independant_mandatory_keywords = add_plural_words(independant_mandatory_keywords)
	dependant_mandatory_keywords = add_plural_words(dependant_mandatory_keywords)
	independant_optional_keywords = add_plural_words(independant_optional_keywords)
	dependant_optional_keywords = add_plural_words(dependant_optional_keywords)
	
	print("SEARCH IS DONE....")

	if independant_mandatory_query and dependant_mandatory_optional_query and \
		not independant_mandatory_optional_query and not dependant_mandatory_query:

		common_patent_ids = get_common_patent_ids(independant_mandatory_patent_ids, \
				dependant_mandatory_optional_patent_ids)
		
		for patent_id in common_patent_ids:
			similar_patent_ids.append({'id':patent_id, 'is_indep':None})
		
		for patent_id in independant_mandatory_patent_ids:
			similar_patent_ids.append({'id':patent_id, 'is_indep':None})
		
	elif dependant_mandatory_query and independant_mandatory_optional_query and \
		not dependant_mandatory_optional_query and not independant_mandatory_query:

		common_patent_ids = get_common_patent_ids(dependant_mandatory_patent_ids, \
				independant_mandatory_optional_patent_ids)
		
		for patent_id in common_patent_ids:
			similar_patent_ids.append({'id':patent_id, 'is_indep':None})
		
		for patent_id in dependant_mandatory_patent_ids:
			similar_patent_ids.append({'id':patent_id, 'is_indep':None})
		
	else:
		if independant_mandatory_response and \
			dependant_mandatory_response and \
			request.POST.get('indep_dep') == "and":
			
			mandatory_patent_ids = get_common_patent_ids(independant_mandatory_patent_ids, \
					dependant_mandatory_patent_ids)
			
			for patent_id in mandatory_patent_ids:
				similar_patent_ids.append({'id':patent_id, 'is_indep':None})
			
			optional_patent_ids = get_common_patent_ids(independant_mandatory_optional_patent_ids, \
					dependant_mandatory_optional_patent_ids)

			for patent_id in optional_patent_ids:
				similar_patent_ids.append({'id':patent_id, 'is_indep':None})
			
		else:

			for patent_id in independant_mandatory_patent_ids:
				similar_patent_ids.append({'id':patent_id, 'is_indep':True})

			for patent_id in dependant_mandatory_patent_ids:
				similar_patent_ids.append({'id':patent_id, 'is_indep':False})
			
			if independant_mandatory_optional_query:
				for patent_id in independant_mandatory_optional_patent_ids:
					similar_patent_ids.append({'id':patent_id, 'is_indep':True})
				
			if dependant_mandatory_optional_query:
				for patent_id in dependant_mandatory_optional_patent_ids:
					similar_patent_ids.append({'id':patent_id, 'is_indep':False})

	similar_patents = remove_repeated_patents(similar_patent_ids)
	
	similar_patents = remove_withdrawn_patents(similar_patents)

	print(ipc_class)

	if ipc_class:
		similar_patents = filter_ipc_class(similar_patents, ipc_class)

	similar_patents = sort_patents(similar_patents, 
						independant_mandatory_keywords,
						independant_optional_keywords,
						dependant_mandatory_keywords,
						dependant_optional_keywords)
	
	similar_patents = prepare_similar_patents(similar_patents)

	request.session['similar_patents'] = similar_patents
	request.session['n_patents'] = len(similar_patents)
	context = {'similar_patents':similar_patents}   
	context['n_patents'] = len(similar_patents)
	 
	if independant_mandatory_keywords:
		independant_mandatory_keywords = ','.join(independant_mandatory_keywords)
	if independant_optional_keywords:
		independant_optional_keywords = ','.join(independant_optional_keywords)
	if dependant_mandatory_keywords:
		dependant_mandatory_keywords = ','.join(dependant_mandatory_keywords)
	if dependant_optional_keywords:
		dependant_optional_keywords = ','.join(dependant_optional_keywords)

	context['independant_mandatory_keywords'] = independant_mandatory_keywords
	context['independant_optional_keywords'] = independant_optional_keywords
	context['dependant_mandatory_keywords'] = dependant_mandatory_keywords
	context['dependant_optional_keywords'] = dependant_optional_keywords
	return context

def preprocess_word_list(words):

	for i in range(0, len(words)):
		words[i] = words[i].lower()
		words[i] = words[i].replace('_', ' ')

	return words

def check_keywords(independant_mandatory_keywords,
	dependant_mandatory_keywords,
	independant_optional_keywords,
	dependant_optional_keywords):

	if not independant_mandatory_keywords[0][0] and \
		not dependant_mandatory_keywords[0][0]:
		
		return False
	else:
		return True

def get_plural(word):

	blob = TextBlob(word)
	plural = blob.words[0].pluralize()
	return plural


def find_synonyms(request):

	if request.method == 'POST':
		keyword = request.POST.get('keyword')
		keyword = keyword.lower()
		keyword = keyword.replace('_', ' ')

		response_data = {}

		try:
			spacy_synonyms = get_spacy_synonyms(keyword)
		
			#wn_synonyms = get_wordnet_synonyms(keyword)
			db_synonyms = get_db_synonyms(keyword)
			synonyms = spacy_synonyms + db_synonyms
			synonyms = preprocess_word_list(synonyms)
			plural = get_plural(keyword)
			synonyms.remove(plural)

			response_data['result'] = 'Successful!'
			response_data['synonyms'] = synonyms
		except:
			response_data['result'] = 'Error!'
			response_data['synonyms'] = []

		return HttpResponse(
			json.dumps(response_data),
			content_type="application/json"
		)
	else:
		return HttpResponse(
			json.dumps({"nothing to see": "this isn't happening"}),
			content_type="application/json"
		)


def find_synonyms_(request):

	if request.method == 'POST':
		keyword = request.POST.get('keyword')
		keyword = keyword.lower()
		keyword = keyword.replace('_', ' ')

		response_data = {}

		api_data = get_api_synonyms(keyword)
		api_synonyms = api_data['synonyms']
		api_general_types = api_data['general_types']
		api_special_types = api_data['special_types']
		
		#wn_synonyms = get_wordnet_synonyms(keyword)
		db_synonyms = get_db_synonyms(keyword)
		synonyms = api_synonyms + db_synonyms
		synonyms = preprocess_word_list(synonyms)

		db_general_types = get_db_general_types(keyword)
		general_types = api_general_types + db_general_types
		general_types = preprocess_word_list(general_types)

		db_special_types = get_db_special_types(keyword)
		special_types = api_special_types + db_special_types
		special_types = preprocess_word_list(special_types)

		response_data['result'] = 'Successful!'
		response_data['synonyms'] = synonyms
		response_data['general_types'] = general_types
		response_data['special_types'] = special_types

		return HttpResponse(
			json.dumps(response_data),
			content_type="application/json"
		)
	else:
		return HttpResponse(
			json.dumps({"nothing to see": "this isn't happening"}),
			content_type="application/json"
		)

def get_patent(request):

	if request.method == 'POST':

		patent_id = request.POST.get('patent_id')
		is_indep = bool(request.POST.get('is_indep'))

		independant_mandatory_keywords = request.POST.get('independant_mandatory_keywords').split(',')
		independant_optional_keywords = request.POST.get('independant_optional_keywords').split(',')
		dependant_mandatory_keywords = request.POST.get('dependant_mandatory_keywords').split(',')
		dependant_optional_keywords = request.POST.get('dependant_optional_keywords').split(',')

		response_data = {}

		patent = Patent.objects.get(pk=patent_id)
		indep_claims = [claim.text for claim in Claim.objects.filter(patent=patent, is_independant=True)]
		dep_claims = [claim.text for claim in Claim.objects.filter(patent=patent, is_independant=False)]

		if is_indep == "True":
			if independant_mandatory_keywords:
				indep_claims = color_claims(independant_mandatory_keywords, independant_optional_keywords, \
					indep_claims, colors_table[0], colors_table[1])
		elif is_indep == "False":
			if dependant_mandatory_keywords:
				dep_claims = color_claims(dependant_mandatory_keywords, dependant_optional_keywords, \
					dep_claims, colors_table[2], colors_table[3])
		else:
			indep_claims = color_claims(independant_mandatory_keywords, independant_optional_keywords, \
				indep_claims, colors_table[0], colors_table[1])
			dep_claims = color_claims(dependant_mandatory_keywords, dependant_optional_keywords, \
				dep_claims, colors_table[2], colors_table[3])

		response_data['title'] = patent.title
		response_data['publication_number'] = patent.publication_number
		response_data['ipc'] = patent.ipc[1:]
		response_data['applicants'] = patent.applicants
		response_data['date'] = patent.date
		response_data['indep_claims'] = indep_claims
		response_data['dep_claims'] = dep_claims

		response_data['result'] = 'Successful!'

		return HttpResponse(
			json.dumps(response_data),
			content_type="application/json"
		)
	else:
		return HttpResponse(
			json.dumps({"nothing to see": "this isn't happening"}),
			content_type="application/json"
		)

def write_to_pdf(patents, output):

	pdf = FPDF()

	for pat in patents:

		patent_id = pat['id']
		comments = pat['comment']

		patent = Patent.objects.get(pk=patent_id)
		
		pdf.add_page()

		pdf.set_font('Arial', 'B', 24)
		pdf.multi_cell(190, 10, txt=patent.title, align="C")
		pdf.ln(h = '')
		pdf.ln(h = '')

		pdf.set_font('Arial', size=12)
		pdf.multi_cell(190, 5, txt=comments)
		pdf.ln(h = '')
		pdf.ln(h = '')

		pdf.multi_cell(190, 5, patent.publication_number)
		pdf.ln(h = '')

		pdf.multi_cell(190, 5, patent.ipc)
		pdf.ln(h = '')

		pdf.multi_cell(190, 5, patent.applicants)
		pdf.ln(h = '')

		pdf.multi_cell(190, 5, patent.date)
		pdf.ln(h = '')
		pdf.ln(h = '')

		indep_claims = [claim.text for claim in Claim.objects.filter(patent=patent, is_independant=True)]
		dep_claims = [claim.text for claim in Claim.objects.filter(patent=patent, is_independant=False)]

		pdf.set_font('Arial', 'B', size=12)
		pdf.multi_cell(190, 5, "Independant Claims:")
		pdf.ln(h = '')

		pdf.set_font('Arial', size=12)
		for claim in indep_claims:
			claim = claim.replace('“', '"')
			claim = claim.replace('”', '"')
			claim = claim.encode("ascii", "replace").decode()
			
			pdf.multi_cell(190, 5, claim)
			pdf.ln(h = '')
		pdf.ln(h = '')

		pdf.set_font('Arial', 'B', size=12)
		pdf.multi_cell(190, 5, "Dependant Claims:")
		pdf.ln(h = '')

		pdf.set_font('Arial', size=12)
		for claim in dep_claims:
			claim = claim.replace('“', '"')
			claim = claim.replace('”', '"')

			claim = claim.encode("ascii", "replace").decode()
			
			pdf.multi_cell(190, 5, claim)
			pdf.ln(h = '')
		pdf.ln(h = '')
	
	pdf.output(output, 'F')#.encode('latin-1')

def generate_report(request):

	if request.method == 'POST':
		
		try:
			patents = request.POST
			patents = list(patents.keys())[0]
			patents = json.loads(patents)
			
			file_dir = get_download_folder()
			file_name = str(datetime.datetime.now())
			output = file_dir + '/' + file_name + '.pdf'

			write_to_pdf(patents, output)
			
			response_data = {'success':True, 'output':output}
			return HttpResponse(
				json.dumps(response_data),
				content_type="application/json"
			)
		except:
			response_data = {'success':False}
			return HttpResponse(
				json.dumps(response_data),
				content_type="application/json"
			)

	else:
		return HttpResponse(
			json.dumps({"nothing to see": "this isn't happening"}),
			content_type="application/json"
		)


