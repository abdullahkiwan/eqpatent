
from .models import Patent, Claim, IndependantClaimIndex, DependantClaimIndex

from elasticsearch_dsl.connections import connections
from elasticsearch.helpers import bulk
from elasticsearch_dsl import Search, Q
from elasticsearch_dsl import DocType, Text, Date
from elasticsearch import Elasticsearch

from operator import itemgetter 
from textblob import TextBlob
import nltk
import time
import re

withdrawn_patents_dir = 'deep_search/withdrawn.txt'

#connection = "https://search-dependant-claims-n4nx74ehli7b6trgzhn6ojg3ky.eu-central-1.es.amazonaws.com"
#connections.create_connection(hosts=[connection])
connections.create_connection()

colors_table = ['#ff0000', '#0040ff', '#80ff00', '#00bfff']

def independant_claim_indexing():

    client = Elasticsearch()

    s = Search(using=client)
    print(len(s))
    return
    for hit in s:
        print(hit)

    IndependantClaimIndex.init()
      
    es = Elasticsearch()
    bulk(client=es, actions=(c.ind_claim_indexing() for c in Claim.objects.filter(is_independant=True).iterator()))

def dependant_claim_indexing():
    DependantClaimIndex.init()
    
    es = Elasticsearch()
    bulk(client=es, actions=(c.dep_claim_indexing() for c in Claim.objects.filter(is_independant=False).iterator()))

def add_synonyms_to_keyword_list(synonyms_list):

    if not synonyms_list: return None

    keywords_list = []
    for synonyms in synonyms_list:
        for word in synonyms:
            
            # Order is very important for a correct highlighting ...        
            if synonyms[word]:
                for syn in synonyms[word]['synonyms']:
                    keywords_list.append(syn)
                for syn in synonyms[word]['additional_synonyms']:
                    keywords_list.append(syn)
            keywords_list.append(word)

    return list(set(keywords_list))

def to_query(text):
    return Q("match_phrase", text=text)

def get_plural(word):

    blob = TextBlob(word)
    plural = blob.words.pluralize()
    if len(plural) == 1:
        return plural[0]
    else:
        return ' '.join(word.split(' ')[:-1]) + ' ' + plural[-1]

def old_prepare_query(mandatory_keywords=None, optional_keywords=None, with_syn=True):

    query = None
    
    query = to_query("camera") & to_query("vision")
    return query

    if mandatory_keywords:
    
        for word, synonyms in mandatory_keywords.items():

            # Temporal query
            q = to_query(word)
            if synonyms:
                for syn in synonyms:
                    q = q | to_query(syn)

            # Adding plural keywords
            q = q | to_query(get_plural(word))
            
            if query: 
                query = query & q
            else: 
                query = q
            
        return query

    if optional_keywords:
        
        for word, synonyms in optional_keywords.items():

            # Temporal query
            q = to_query(word)
            if synonyms:
                for syn in synonyms:
                    q = q | to_query(syn)

            # Adding plural keywords
            q = q | to_query(get_plural(word))
            q = ( q )

            query = query & q
            
        return query
    return None

def prepare_query(mandatory_keywords_list=None, optional_keywords_list=None):

    mandatory_keywords_query = None

    if mandatory_keywords_list:
        
        for mandatory_keywords in mandatory_keywords_list:

            keyword_group_query = None
            for word, synonyms in mandatory_keywords.items():

                # Temporal query
                word_query = to_query(word)
                if synonyms:
                    for syn in synonyms['synonyms']:
                        word_query = word_query | to_query(syn)
                    for syn in synonyms['additional_synonyms']:
                        word_query = word_query | to_query(syn)
                # Adding plural keywords
                word_query = word_query | to_query(get_plural(word))
                
                word_query = ( word_query )

                if keyword_group_query: 
                    keyword_group_query = keyword_group_query & word_query
                else: 
                    keyword_group_query = word_query
            
            if mandatory_keywords_query:
                mandatory_keywords_query = mandatory_keywords_query | keyword_group_query
            else:
                mandatory_keywords_query = keyword_group_query

    optional_keywords_query = None

    if optional_keywords_list:
        
        for optional_keywords in optional_keywords_list:

            keyword_group_query = None
            for word, synonyms in optional_keywords.items():

                # Temporal query
                word_query = to_query(word)
                if synonyms:
                    for syn in synonyms['synonyms']:
                        word_query = word_query | to_query(syn)
                    for syn in synonyms['additional_synonyms']:
                        word_query = word_query | to_query(syn)

                # Adding plural keywords
                word_query = word_query | to_query(get_plural(word))
                word_query = ( word_query )

                if keyword_group_query: 
                    keyword_group_query = keyword_group_query & word_query
                else: 
                    keyword_group_query = word_query

            if optional_keywords_query:
                optional_keywords_query = optional_keywords_query | keyword_group_query
            else:
                optional_keywords_query = keyword_group_query

    if mandatory_keywords_query and optional_keywords_query:
        return mandatory_keywords_query & optional_keywords_query
    elif mandatory_keywords_query:
        return mandatory_keywords_query
    else:
        return optional_keywords_query

def search(query, is_indep=True):
    
    if query:
        
        if is_indep: index='independant-claim'
        else: index='dependant-claim'
        
        if is_indep: s = Search(index='independant-claim')
        else: s = Search(index='dependant-claim')
    
        s = s.query(query)
        response = s.scan()

        return response
        
    else:
        return None

def score_similar_patent(claims, keywords):
    
    score = 0
    
    if keywords:
        for claim in claims:
            for word in keywords:
                score += len([m.start() for m in re.finditer(word, claim.lower())])
                     
    return score

def sort_patents(similar_patents, 
                    independant_mandatory_keywords,
                    independant_optional_keywords,
                    dependant_mandatory_keywords,
                    dependant_optional_keywords):
    
    for idx in range(len(similar_patents)):

        patent_id = similar_patents[idx]['id']
        patent = Patent.objects.get(pk=patent_id)
        
        indep_claims = [claim.text for claim in Claim.objects.filter(patent=patent, is_independant=True)]
        dep_claims = [claim.text for claim in Claim.objects.filter(patent=patent, is_independant=False)]
        
        similar_patents[idx]['mandatory_score'] = score_similar_patent(indep_claims, 
                                                    independant_mandatory_keywords)
        similar_patents[idx]['mandatory_score'] += score_similar_patent(dep_claims, 
                                                    dependant_mandatory_keywords)

        similar_patents[idx]['optional_score'] = score_similar_patent(indep_claims, 
                                                    independant_optional_keywords)
        similar_patents[idx]['optional_score'] += score_similar_patent(dep_claims, 
                                                    dependant_optional_keywords)

    return sorted(similar_patents, key=itemgetter('optional_score', 'mandatory_score'), reverse=True) 

def color_claims(mandatory_keywords, optional_keywords, claims, mand_color, opt_color):
    
    colored_claims = []
    for claim in claims:
        
        for word in mandatory_keywords:
            claim = re.sub(word, r'<span style="color:'+mand_color+'">'+word+'</span>', claim, flags=re.I)

        if optional_keywords:
            for word in optional_keywords:
                claim = re.sub(word, r'<span style="color:'+opt_color+'">'+word+'</span>', claim, flags=re.I)

        colored_claims.append(claim)

    return colored_claims

def get_similar_patent_ids(response):

    if not response: return []

    patent_ids = []

    for hit in response:   
        patent_id = hit['patent']
        if patent_id not in patent_ids: patent_ids.append(patent_id)

    return patent_ids

def add_plural_words(keywords):

    if not keywords: return None

    keywords_with_plurals = []
    for word in keywords:
        keywords_with_plurals.append(get_plural(word))

    keywords_with_plurals += keywords

    return keywords_with_plurals

def prepare_similar_patents(similar_patents):

    for idx in range(len(similar_patents)):

        patent_id = similar_patents[idx]['id']
        patent = Patent.objects.get(pk=patent_id)

        similar_patents[idx]['title'] = patent.title
        similar_patents[idx]['publication_number'] = patent.publication_number
        similar_patents[idx]['ipc'] = patent.ipc[1:]
        similar_patents[idx]['applicants'] = patent.applicants
        similar_patents[idx]['date'] = patent.date

    return similar_patents

def remove_repeated_patents(patents):
    n = 0
    filtered_patents = []
    found_patent_ids = []

    for patent in patents:
        if patent['id'] not in found_patent_ids:
           filtered_patents.append(patent)
           found_patent_ids.append(patent['id'])
        else:
            n += 1

            for idx in range(len(filtered_patents)):
                if patent['id'] == filtered_patents[idx]['id'] and \
                    patent['is_indep'] != filtered_patents[idx]['is_indep']:
                    filtered_patents[idx]['is_indep'] = None
 
    return filtered_patents 

def get_common_patent_ids(independant_results, dependant_results):
    
    return list(set(independant_results).intersection(dependant_results))

def merge_two_dicts(x, y):
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    return z

def get_withdrawn_patents(dir):

    patent_numbers = []
    f = open(dir, 'r')
    for patent_number in f.readlines():
        patent_numbers.append(patent_number.rstrip())
    return patent_numbers

def remove_withdrawn_patents(patents):

    withdrawn_patents = get_withdrawn_patents(withdrawn_patents_dir)

    filtered_patents = []
    for patent in patents:
        p = Patent.objects.get(pk=patent['id'])
        publ_number = p.publication_number.split('-')[0][2:]
        if publ_number not in withdrawn_patents:
            filtered_patents.append(patent)

    return filtered_patents

def filter_ipc_class(patents, ipc_class):

    filtered_patents = []

    for patent in patents:

        p = Patent.objects.get(pk=patent['id'])
        if p.ipc and p.ipc[1:].startswith(ipc_class):
            filtered_patents.append(patent)

    return filtered_patents

