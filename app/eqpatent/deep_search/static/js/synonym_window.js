/*

	jQuery Tags Input Plugin 1.3.3

	Copyright (c) 2011 XOXCO, Inc

	Documentation for this plugin lives here:
	http://xoxco.com/clickable/jquery-tags-input

	Licensed under the MIT license:
	http://www.opensource.org/licenses/mit-license.php

	ben@xoxco.com

*/

var synonyms;
var success_ajax = false;

var saved_synonyms = {};
var saved_modals = [];

var n_keywords = 0;
var n_fetched_synonyms = 0;

var warningShown = false;
var successShown = false;

function check_fetched_synonyms() {

	if(n_keywords == n_fetched_synonyms && n_keywords!=0)
	{
		document.getElementById("search_btn").disabled = false;
		
		if(!successShown)
		{
			$("#synonym_notifications").empty();

			var syn_success = '<div class="alert alert-success appear-animation animated fadeIn appear-animation-visible" style="margin-top: 1rem;">';
			syn_success += 'Synonyms were generated successfully.';
			syn_success += '</div>';

			$("#synonym_notifications").append(syn_success);
			successShown = true;
		}

		warningShown = false;
	}
	else if(n_keywords != 0)
	{
		document.getElementById("search_btn").disabled = true;
		successShown = false;

		if(!warningShown)
		{
			var syn_alert = '<div class="alert alert-warning appear-animation animated fadeIn appear-animation-visible" style="margin-top: 1rem;">';
			syn_alert += 'Synonyms are being generated.';
			syn_alert += '</div>';
			warningShown = true;

			$("#synonym_notifications").empty();
			$("#synonym_notifications").append(syn_alert);
		}
	}
}


(function($) {

	var delimiter = new Array();
	var tags_callbacks = new Array();
	$.fn.doAutosize = function(o){
	    var minWidth = $(this).data('minwidth'),
	        maxWidth = $(this).data('maxwidth'),
	        val = '',
	        input = $(this),
	        testSubject = $('#'+$(this).data('tester_id'));

	    if (val === (val = input.val())) {return;}

	    // Enter new content into testSubject
	    var escaped = val.replace(/&/g, '&amp;').replace(/\s/g,' ').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	    testSubject.html(escaped);
	    // Calculate new width + whether to change
	    var testerWidth = testSubject.width(),
	        newWidth = (testerWidth + o.comfortZone) >= minWidth ? testerWidth + o.comfortZone : minWidth,
	        currentWidth = input.width(),
	        isValidWidthChange = (newWidth < currentWidth && newWidth >= minWidth)
	                             || (newWidth > minWidth && newWidth < maxWidth);

	    // Animate width
	    if (isValidWidthChange) {
	        input.width(newWidth);
	    }
  	};

	$.fn.resetAutosize = function(options){
		// alert(JSON.stringify(options));
		var minWidth =  $(this).data('minwidth') || options.minInputWidth || $(this).width(),
		    maxWidth = $(this).data('maxwidth') || options.maxInputWidth || ($(this).closest('.tagsinput').width() - options.inputPadding),
		    val = '',
		    input = $(this),
		    testSubject = $('<tester/>').css({
		        position: 'absolute',
		        top: -9999,
		        left: -9999,
		        width: 'auto',
		        fontSize: input.css('fontSize'),
		        fontFamily: input.css('fontFamily'),
		        fontWeight: input.css('fontWeight'),
		        letterSpacing: input.css('letterSpacing'),
		        whiteSpace: 'nowrap'
		    }),
		    testerId = $(this).attr('id')+'_autosize_tester';
		if(! $('#'+testerId).length > 0){
		  testSubject.attr('id', testerId);
		  testSubject.appendTo('body');
		}

		input.data('minwidth', minWidth);
		input.data('maxwidth', maxWidth);
		input.data('tester_id', testerId);
		input.css('width', minWidth);
	};

	$.fn.addTag = function(value,options) {

			var id = $(this).attr('id');

			options = jQuery.extend({focus:false,callback:true},options);
			this.each(function() {
				
				var tagslist = $(this).val().split(delimiter[id]);
				if (tagslist[0] == '') {
					tagslist = new Array();
				}

				value = jQuery.trim(value);

				if (options.unique) {
					var skipTag = $(this).tagExist(value);
					if(skipTag == true) {
					    //Marks fake input as not_valid to let styling it
    				    $('#'+id+'_tag').addClass('not_valid');
    				}
				} else {
					var skipTag = false;
				}

				if (value !='' && skipTag != true) {

                    var new_span = $('<span>').addClass('tag');
                    new_span.append(

                    	$('<a><img id="delete_btn" src="/static/img/delete.jpg" title="Delete word">', {
                            href  : '#',
                            title : 'Remove Keyword',
                            text  : 'x '
                        }).click(function () {
                            
							return $('#' + id).removeTag(escape(value));
							
                        }).css({fontSize: 15}),

                        $('<span>').text(value).append('')

                    ).insertBefore('#' + id + '_addTag');

                    if(!id.includes("synonyms") && !id.includes("general_types") && !id.includes("special_types"))
					{
						var new_span_html = '<a><img id="synonyms_btn" src="/static/img/synonyms.jpg" title="Synonyms : 0" data-toggle="modal" data-target="#';
						new_span_html += id;
						new_span_html += '_info_';
						new_span_html += value;
						new_span_html += '"></a>';
						new_span.append(

	                    	$(new_span_html).click(function () {
	                           
	                        	if ( value.split(" ").length > 1)
									value = value.split(" ").join("_");

	                        	//modal_window = document.getElementById(id+'_info_'+value);
								//modal_window.style.display = "inline-block";

	                        }).css({fontSize: 15}),

	                    );
					}

					if(!id.includes("synonyms") && !id.includes("general_types") && !id.includes("special_types"))
					{
						var and_span = $('<span>').addClass('and_tag').attr('id', id+'_'+value+'_and');
                    	and_span.append(

                        	$('<span>').text("AND").append('')

                    	).insertBefore('#' + id + '_addTag');
                    }

					tagslist.push(value);

					$('#'+id+'_tag').val('');
					if (options.focus) {
						$('#'+id+'_tag').focus();
					} else {
						$('#'+id+'_tag').blur();
					}

					$.fn.tagsInput.updateTagsField(this,tagslist);

					if (options.callback && tags_callbacks[id] && tags_callbacks[id]['onAddTag']) {
						var f = tags_callbacks[id]['onAddTag'];
						f.call(this, value);
					}
					if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
					{
						var i = tagslist.length;
						var f = tags_callbacks[id]['onChange'];
						f.call(this, $(this), tagslist[i-1]);
					}
				}

			});

			if(value != '' && !id.includes("synonyms"))		
				if ( ! $('#'+id+'_info_'+value).length )
					fetch_synonyms(value, $(this).attr('id'));
			
			return false;
		};

	$.fn.removeTag = function(value) {
			value = unescape(value);
			this.each(function() {
				var id = $(this).attr('id');

				var old = $(this).val().split(delimiter[id]);

				$('#'+id+'_tagsinput .and_tag').remove();

				$('#'+id+'_tagsinput .tag').remove();
				str = '';
				for (i=0; i< old.length; i++) {
					if (old[i]!=value) {
						str = str + delimiter[id] +old[i];
					}
				}

				$.fn.tagsInput.importTags(this,str);

				if (tags_callbacks[id] && tags_callbacks[id]['onRemoveTag']) {
					var f = tags_callbacks[id]['onRemoveTag'];
					f.call(this, value);
				}

			});

			return false;
		};

	$.fn.tagExist = function(val) {
		var id = $(this).attr('id');
		var tagslist = $(this).val().split(delimiter[id]);
		return (jQuery.inArray(val, tagslist) >= 0); //true when tag exists, false when not
	};

   // clear all existing tags and import new ones from a string
   $.fn.importTags = function(str) {
      var id = $(this).attr('id');
      $('#'+id+'_tagsinput .tag').remove();
      $.fn.tagsInput.importTags(this,str);
   }

	$.fn.tagsInput = function(options) {

    var settings = jQuery.extend({
      interactive:true,
      defaultText:'',
      minChars:0,
      width:'300px',
      height:'100px',
      autocomplete: {selectFirst: false },
      hide:true,
      delimiter: ',',
      unique:true,
      removeWithBackspace:true,
      placeholderColor:'#666666',
      autosize: true,
      comfortZone: 20,
      inputPadding: 6*2
    },options);

    	var uniqueIdCounter = 0;

		this.each(function() {
         // If we have already initialized the field, do not do it again
         if (typeof $(this).attr('data-tagsinput-init') !== 'undefined') {
            return;
         }

         // Mark the field as having been initialized
         $(this).attr('data-tagsinput-init', true);

			if (settings.hide) {
				$(this).hide();
			}
			var id = $(this).attr('id');
			if (!id || delimiter[$(this).attr('id')]) {
				id = $(this).attr('id', 'tags' + new Date().getTime() + (uniqueIdCounter++)).attr('id');
			}

			var data = jQuery.extend({
				pid:id,
				real_input: '#'+id,
				holder: '#'+id+'_tagsinput',
				input_wrapper: '#'+id+'_addTag',
				fake_input: '#'+id+'_tag'
			},settings);

			delimiter[id] = data.delimiter;

			if (settings.onAddTag || settings.onRemoveTag || settings.onChange) {
				tags_callbacks[id] = new Array();
				tags_callbacks[id]['onAddTag'] = settings.onAddTag;
				tags_callbacks[id]['onRemoveTag'] = settings.onRemoveTag;
				tags_callbacks[id]['onChange'] = settings.onChange;
			}

			var markup = '<div id="'+id+'_tagsinput" class="tagsinput"><div id="'+id+'_addTag">';

			if (settings.interactive) {
				markup = markup + '<input id="'+id+'_tag" value="" data-default="'+settings.defaultText+'" />';
			}

			markup = markup + '</div><div class="tags_clear"></div></div>';

			$(markup).insertAfter(this);

			$(data.holder).css('width',settings.width);
			$(data.holder).css('min-height',settings.height);
			$(data.holder).css('height',settings.height);

			if ($(data.real_input).val()!='') {
				$.fn.tagsInput.importTags($(data.real_input),$(data.real_input).val());
			}
			if (settings.interactive) {
				$(data.fake_input).val($(data.fake_input).attr('data-default'));
				$(data.fake_input).css('color',settings.placeholderColor);
		        $(data.fake_input).resetAutosize(settings);

				$(data.holder).bind('click',data,function(event) {
					$(event.data.fake_input).focus();
				});

				$(data.fake_input).bind('focus',data,function(event) {
					if ($(event.data.fake_input).val()==$(event.data.fake_input).attr('data-default')) {
						$(event.data.fake_input).val('');
					}
					$(event.data.fake_input).css('color','#000000');
				});

				if (settings.autocomplete_url != undefined) {
					autocomplete_options = {source: settings.autocomplete_url};
					for (attrname in settings.autocomplete) {
						autocomplete_options[attrname] = settings.autocomplete[attrname];
					}

					if (jQuery.Autocompleter !== undefined) {
						$(data.fake_input).autocomplete(settings.autocomplete_url, settings.autocomplete);
						$(data.fake_input).bind('result',data,function(event,data,formatted) {
							if (data) {
								$('#'+id).addTag(data[0] + "",{focus:true,unique:(settings.unique)});
							}
					  	});
					} else if (jQuery.ui.autocomplete !== undefined) {
						$(data.fake_input).autocomplete(autocomplete_options);
						$(data.fake_input).bind('autocompleteselect',data,function(event,ui) {
							$(event.data.real_input).addTag(ui.item.value,{focus:true,unique:(settings.unique)});
							return false;
						});
					}


				} else {
						// if a user tabs out of the field, create a new tag
						// this is only available if autocomplete is not used.
						$(data.fake_input).bind('blur',data,function(event) {
							var d = $(this).attr('data-default');
							if ($(event.data.fake_input).val()!='' && $(event.data.fake_input).val()!=d) {
								if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
									$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
							} else {
								$(event.data.fake_input).val($(event.data.fake_input).attr('data-default'));
								$(event.data.fake_input).css('color',settings.placeholderColor);
							}
							return false;
						});

				}
				// if user types a default delimiter like comma,semicolon and then create a new tag
				$(data.fake_input).bind('keypress',data,function(event) {
					if (_checkDelimiter(event)) {
					    event.preventDefault();
						if( (event.data.minChars <= $(event.data.fake_input).val().length) && (!event.data.maxChars || (event.data.maxChars >= $(event.data.fake_input).val().length)) )
							$(event.data.real_input).addTag($(event.data.fake_input).val(),{focus:true,unique:(settings.unique)});
					  	$(event.data.fake_input).resetAutosize(settings);
						return false;
					} else if (event.data.autosize) {
			            $(event.data.fake_input).doAutosize(settings);

          			}
				});
				//Delete last tag on backspace
				data.removeWithBackspace && $(data.fake_input).bind('keydown', function(event)
				{
					if(event.keyCode == 8 && $(this).val() == '')
					{
						 event.preventDefault();
						 var last_tag = $(this).closest('.tagsinput').find('.tag:last').text();
						 var id = $(this).attr('id').replace(/_tag$/, '');
						 last_tag = last_tag.replace(/[\s]+x$/, '');
						 $('#' + id).removeTag(escape(last_tag));
						 $(this).trigger('focus');
					}
				});
				$(data.fake_input).blur();

				//Removes the not_valid class when user changes the value of the fake input
				if(data.unique) {
				    $(data.fake_input).keydown(function(event){
				        if(event.keyCode == 8 || String.fromCharCode(event.which).match(/\w+|[áéíóúÁÉÍÓÚñÑ,/]+/)) {
				            $(this).removeClass('not_valid');
				        }
				    });
				}
			} // if settings.interactive
		});

		return this;

	};

	$.fn.tagsInput.updateTagsField = function(obj,tagslist) {
		var id = $(obj).attr('id');
		$(obj).val(tagslist.join(delimiter[id]));
	};

	$.fn.tagsInput.importTags = function(obj,val) {
		$(obj).val('');
		var id = $(obj).attr('id');
		var tags = val.split(delimiter[id]);
		for (i=0; i<tags.length; i++) {
			$(obj).addTag(tags[i],{focus:false,callback:false});
		}
		if(tags_callbacks[id] && tags_callbacks[id]['onChange'])
		{
			var f = tags_callbacks[id]['onChange'];
			f.call(obj, obj, tags[i]);
		}
	};

   /**
     * check delimiter Array
     * @param event
     * @returns {boolean}
     * @private
     */
   var _checkDelimiter = function(event){
      var found = false;
      if (event.which == 13) {
         return true;
      }

      if (typeof event.data.delimiter === 'string') {
         if (event.which == event.data.delimiter.charCodeAt(0)) {
            found = true;
         }
      } else {
         $.each(event.data.delimiter, function(index, delimiter) {
            if (event.which == delimiter.charCodeAt(0)) {
               found = true;
            }
         });
      }

      return found;
   }
})(jQuery);

function retrieve_synonyms_from_userinput(user_input, keyword, id) {

	var ids = id.split('_');
	var idx = ids[2]-1;

	var additional_synonyms = user_input[idx][keyword]['additional_synonyms']
	$('#'+id+'_additional_synonyms_'+keyword).importTags(additional_synonyms.join());


	var check_inputs = $("input[id^='"+id+"_synonyms_"+keyword+"']" );
	
	
	for(var i = 0 ; i < check_inputs.length ; i++)
		if(user_input[idx][keyword]['synonyms'].includes(check_inputs[i].value))
			check_inputs[i].checked = true;

	save_synonyms(id, keyword);
}

function cal_synonyms(id, keyword) {

	modal_id = id+'_info_'+keyword;

	var checked = $("#"+modal_id+" :checkbox:checked").length;
	var modal = document.getElementById(modal_id);
	var additional_synonyms = modal.getElementsByClassName('tag').length;
	
	var n = checked + additional_synonyms;

	var btn = document.getElementById('synonyms_btn');
	btn.setAttribute('Title', "Synonyms : "+n);
}


function fetch_synonyms(keyword, id) {
	
	$("#error_notifications").empty();

	n_keywords += 1;

	check_fetched_synonyms();

    console.log("Fetching synonyms is working!") // sanity check
    $.ajax({
        url : "find_synonyms/", // the endpoint
        type : "POST", // http method
        data : { 'keyword' : keyword }, // data sent with the post request
        dataType: 'json',
        // handle a successful response
        success : function(data) {
        	
        	synonyms = data.synonyms;
			create_synonyms_window(id, keyword, synonyms);

			if (independant_mandatory && id.startsWith('independant_mandatory'))
				retrieve_synonyms_from_userinput(independant_mandatory, keyword, id);
			
			if (independant_optional && id.startsWith('independant_optional'))
				retrieve_synonyms_from_userinput(independant_optional, keyword, id);

			if (dependant_mandatory && id.startsWith('dependant_mandatory'))
				retrieve_synonyms_from_userinput(dependant_mandatory, keyword, id);

			if (dependant_optional && id.startsWith('dependant_optional'))
				retrieve_synonyms_from_userinput(dependant_optional, keyword, id);

			n_fetched_synonyms += 1;

			check_fetched_synonyms();
        }
    });
}


function save_synonyms(id, keyword)
{
	var modal_id = id+'_info_'+keyword;

	var checked = [];
	$('#'+modal_id+' :checkbox:checked').each(function(i)
	{
    	checked[i] = $(this).attr("id");
    });

    saved_synonyms[modal_id]['checked'] = checked;
	saved_synonyms[modal_id]['additional_synonyms'] = $('#'+id+"_additional_synonyms_"+keyword).val();
}

function retrieve_synonyms(id, keyword)
{
	var modal_id = id+'_info_'+keyword;

	var inputs = document.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++) 
	{
		if (inputs[i].id.includes(id) && inputs[i].id.includes(keyword) && inputs[i].type == "checkbox" && inputs[i].checked)
			inputs[i].checked = false;
	}

	var inputs = document.getElementsByTagName("input");
	for (var i = 0; i < inputs.length; i++) 
		if (inputs[i].type == "checkbox")
			if (saved_synonyms[modal_id]['checked'].includes(inputs[i].id))
	    		inputs[i].checked = true;

	$('#'+id+"_additional_synonyms_"+keyword).importTags(saved_synonyms[modal_id]['additional_synonyms']);
}

function close_synonyms_window(id, keyword)
{
	modal_id = id+'_info_'+keyword;
	modal_close = document.getElementById('close_'+modal_id);
	
	modal_close.onclick = function() 
	{
		retrieve_synonyms(id, keyword);
	}
}

function save_synonyms_window(id, keyword)
{
	modal_id = id+'_info_'+keyword;
	modal_save = document.getElementById('save_'+modal_id);
	
	modal_save.onclick = function() {

		save_synonyms(id, keyword);
		cal_synonyms(id, keyword);
	}
}


function create_synonyms_window_(id, keyword, synonyms) 
{
	keyword_with_underscore = keyword
	if ( keyword.split(" ").length > 1)
		keyword_with_underscore = keyword.split(" ").join("_");

	modal_id = id+'_info_'+keyword_with_underscore;

	if ( ! $('#'+id+"_info_"+keyword_with_underscore).length )
	{
		var modal_div = '';

		modal_div += '<div class="modal fade" id="'
		modal_div += modal_id;
		modal_div += '" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">';
		modal_div += '<div class="modal-dialog modal-lg">';
		modal_div += '<div class="modal-content"><div class="modal-header">';
		
		modal_div += '<h4 class="modal-title" id="formModalLabel">';
		modal_div += keyword;
		modal_div += '</h4>';
		
		modal_div += '<div>';
		modal_div += '<button type="button" class="btn btn-light closeModal" data-dismiss="modal" #closeBtn id="close_';
		modal_div += modal_id;
		modal_div += '" name="close_';
		modal_div += modal_id;
		modal_div += '">Close</button>';

		modal_div += '<button type="button" class="btn btn-primary" id="save_';
		modal_div += modal_id;
		modal_div += '" name="save_';
		modal_div += modal_id;
		modal_div += '">Save Changes</button>';
		modal_div += '</div></div>'

		modal_div += '<div class="modal-body"><div class="row center">';
		
		modal_div += '<div class="col-sm-4"><h4>Synonyms</h4>';
		if (synonyms.length > 0)
			for (idx in synonyms)
			{
				modal_div += '<div class="form-row"><div class="col"><div class="custom-control custom-checkbox pb-3">';
				modal_div += '<input type="checkbox" class="custom-control-input" name="';
				modal_div += id;
				modal_div += '_synonyms_';
				modal_div += keyword_with_underscore;
				modal_div += '" ';
				modal_div += 'id = "';
				modal_div += id;
				modal_div += '_synonyms_';
				modal_div += keyword_with_underscore;
				modal_div += '_';
				modal_div += idx;
				modal_div += '">';
				modal_div += '<label class="custom-control-label" for="';
				modal_div += id;
				modal_div += '_synonyms_';
				modal_div += keyword_with_underscore;
				modal_div += '_';
				modal_div += idx;
				modal_div += '">';
				modal_div += synonyms[idx]
				modal_div += '</label></div></div></div>';
			}
		else
			modal_div += '<p>No results</p>';
		modal_div += '</div>';

		modal_div += '</div>';

		var additional_synonyms_id = id + '_additional_synonyms_' + keyword_with_underscore;
		var additional_general_types_id = id + '_additional_general_types_' + keyword_with_underscore;
		var additional_special_types_id = id + '_additional_special_types_' + keyword_with_underscore;

		modal_div += '<div class="col-lg-12">';
		modal_div += '<label class="font-weight-bold text-dark text-2">Synonym hinzufügen</label>';
		modal_div += '<input type="text" value="" class="form-control" name="';
		modal_div += additional_synonyms_id;
		modal_div += '" id="';
		modal_div += additional_synonyms_id;
		modal_div += '"></div>';

		modal_div += '<div class="form-group col-lg-12">';
		modal_div += '<label class="font-weight-bold text-dark text-2">Hyperonym hinzufügen</label>';
		modal_div += '<input type="text" value="" class="form-control" name="';
		modal_div += additional_general_types_id; 
		modal_div += '" id="';
		modal_div += additional_general_types_id;
		modal_div += '"></div>';

		modal_div += '<div class="form-group col-lg-12">';
		modal_div += '<label class="font-weight-bold text-dark text-2">Hyponym hinzufügen</label>';
		modal_div += '<input type="text" value="" class="form-control" name="';
		modal_div += additional_special_types_id; 
		modal_div += '" id="';
		modal_div += additional_special_types_id;
		modal_div += '"></div>';
		modal_div += '</div></div></div>';
					
		modal_div += '</div></div>';

		$("#deep_search_form").append(modal_div);

		$('#'+additional_synonyms_id).tagsInput({width:'auto', height:'60px', placeholder:''});
		$('#'+additional_general_types_id).tagsInput({width:'auto', height:'60px', placeholder:''});
		$('#'+additional_special_types_id).tagsInput({width:'auto', height:'60px', placeholder:''});
		
		modal_window = document.getElementById(modal_id);
		
		saved_synonyms[modal_id] = {}
		saved_synonyms[modal_id]['checked'] = [];
		saved_synonyms[modal_id]['additional_synonyms'] = '';
		saved_synonyms[modal_id]['additional_general_types'] = '';
		saved_synonyms[modal_id]['additional_special_types'] = '';

		modal_close = document.getElementById('close_'+modal_id);
		modal_save = document.getElementById('save_'+modal_id);

		save_synonyms_window(id, keyword_with_underscore);
		close_synonyms_window(id, keyword_with_underscore);
	}
}


function create_synonyms_window(id, keyword, synonyms) 
{
	keyword_with_underscore = keyword
	if ( keyword.split(" ").length > 1)
		keyword_with_underscore = keyword.split(" ").join("_");

	modal_id = id+'_info_'+keyword_with_underscore;

	if ( ! $('#'+id+"_info_"+keyword_with_underscore).length )
	{
		var modal_div = '';

		modal_div += '<div class="modal fade" id="';
		modal_div += modal_id
		modal_div += '" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">';
		modal_div += '<div class="modal-dialog modal-lg">';
		modal_div += '<div class="modal-content">';
		modal_div += '<div class="modal-header">';
		modal_div += '<h4 class="modal-title" id="formModalLabel">';
		modal_div += keyword
		modal_div += '</h4>';
		modal_div += '<div>';
		modal_div += '<button type="button" class="btn btn-light closeModal" data-dismiss="modal" #closeBtn id="close_';
		modal_div += modal_id;
		modal_div += '" name="close_';
		modal_div += modal_id;
		modal_div += '" style="margin-right: 10px;">Close</button>';

		modal_div += '<button type="button" class="btn btn-primary" id="save_';
		modal_div += modal_id;
		modal_div += '" name="save_';
		modal_div += modal_id;
		modal_div += '">Save Changes</button>';
		modal_div += '</div></div>';

		modal_div += '<div class="modal-body">';
		modal_div += '<div class="row">';		

		var end = 0;
		var start = 0;
		if (synonyms.length > 0)
			for(var i = 0 ; i < 3 ; i++)
			{
				start = end;
			
				var end = Math.floor(synonyms.length / 3);
				if (synonyms.length % 3 != 0 && i == 0)
					end += 1;
				if (synonyms.length % 3 == 2 && i == 1)
					end += 1;
				end += start;

				modal_div += '<div class="col-sm-4">';
				for(var idx = start ; idx < end ; idx += 1)
					modal_div += synonym_checkbox(id, keyword_with_underscore, synonyms[idx], idx);
				modal_div += '</div>';
			}
		else
			modal_div += '<div class="form-row"><div class="col"><div class="custom-control pb-3"><p>No results</p></div></div></div>';
		modal_div += '</div>';

		var additional_synonyms_id = id + '_additional_synonyms_' + keyword_with_underscore;
		
		modal_div += '<div class="form-group col-lg-12">';
		modal_div += '<label class="font-weight-bold text-dark text-2">Synonym hinzufügen</label>';
		modal_div += '<input type="text" value="" class="form-control" name="';
		modal_div += additional_synonyms_id;
		modal_div += '" id="';
		modal_div += additional_synonyms_id;
		modal_div += '"></div>';

		modal_div += '</div></div></div></div>';

		$("#deep_search_form").append(modal_div);

		$('#'+additional_synonyms_id).tagsInput({width:'auto', height:'60px', placeholder:''});

		modal_window = document.getElementById(modal_id);
		
		saved_synonyms[modal_id] = {}
		saved_synonyms[modal_id]['checked'] = [];
		saved_synonyms[modal_id]['additional_synonyms'] = '';

		modal_close = document.getElementById('close_'+modal_id);
		modal_save = document.getElementById('save_'+modal_id);

		save_synonyms_window(id, keyword_with_underscore);
		close_synonyms_window(id, keyword_with_underscore);
	}
}

function synonym_checkbox(id, keyword, synonym, idx)
{
	modal_div = '';

	modal_div += '<div class="form-row" style="margin-left: 50px;">';
	modal_div += '<div class="col">';
	modal_div += '<div class="custom-control custom-checkbox pb-3">';
	modal_div += '<input type="checkbox" class="custom-control-input" name="';
	modal_div += id;
	modal_div += '_synonyms_';
	modal_div += keyword;
	modal_div += '" id = "';
	modal_div += id;
	modal_div += '_synonyms_';
	modal_div += keyword;
	modal_div += '_';
	modal_div += idx;
	modal_div += '" value="';
	modal_div += synonym
	modal_div += '">';
			
	modal_div += '<label class="custom-control-label" for="';
	modal_div += id;
	modal_div += '_synonyms_';
	modal_div += keyword;
	modal_div += '_';
	modal_div += idx;
	modal_div += '">';
	modal_div += synonym;
	modal_div += '</label>';
	
	modal_div += '</div>';
	modal_div += '</div>';
	modal_div += '</div>';

	return modal_div;
}

