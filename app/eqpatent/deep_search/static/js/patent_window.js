
function fetch_patent(patent_id, is_indep) {

	console.log(independant_mandatory_keywords);

	data = { 
        	'patent_id' : patent_id,
        	'is_indep' : is_indep,
        	'independant_mandatory_keywords' : independant_mandatory_keywords,
         	'independant_optional_keywords' : independant_optional_keywords,
         	'dependant_mandatory_keywords' : dependant_mandatory_keywords,
         	'dependant_optional_keywords' : dependant_optional_keywords,
        };

    console.log("Fetching patent is working!"); // sanity check
    $.ajax({
        url : "get_patent/", // the endpoint
        type : "POST", // http method
        data : data, // data sent with the post request
        
        dataType: 'json',
        // handle a successful response
        success : function(data) {
			create_patent_window(patent_id, data);
        }
    }); 
}

function create_patent_window(id, data) {
			
	var modal_div = '';

	modal_div += '<div id="patent_';
	modal_div += id;
	modal_div += '" class="modal fade" ';
	modal_div += 'tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">';
	modal_div += '<div class="modal-dialog modal-lg">';
	modal_div += '<div class="modal-content">';
	modal_div += '<div class="modal-header">';
	modal_div += '<h4 class="modal-title" id="formModalLabel">';
	modal_div += data.title;
	modal_div += '</h4>';

	modal_div += '<div>';
	modal_div += '<button type="button" class="btn btn-light closeModal" data-dismiss="modal" #closeBtn id="close_';
	modal_div += id;
	modal_div += '" name="close_'
	modal_div += id;
	modal_div += '">Close</button>';

	modal_div += '</div></div>';

	modal_div += '<div class="modal-body">';

	modal_div += 'IPC Class: ';
	modal_div += data.ipc;
	modal_div += '<br>';
	modal_div += 'Inventors: '
	modal_div += data.applicants;
	modal_div += '<br>';
	modal_div += 'Priority Date: ';
	modal_div += data.date;
	modal_div += '<br>';
	modal_div += 'Publication Number: ';
	modal_div += data.publication_number;
	modal_div += '<br>';	    
	modal_div += '<h6 style="margin-top: 30px;">Independant Claims:</h6>';
    modal_div += '<ul>';
    for (idx in data.indep_claims)
    {
    	modal_div += '<li style="margin-bottom: 10px;">'
    	modal_div += data.indep_claims[idx];
    	modal_div += '</li>';
    }
    modal_div += '</ul>';
    modal_div += '<br>';
    modal_div += '<h6 style="margin-top: 30px;">Dependant Claims:</h6>';
    modal_div += '<ul>';
    for (idx in data.dep_claims)
    {
    	modal_div += '<li style="margin-bottom: 10px;">'
    	modal_div += data.dep_claims[idx];
    	modal_div += '</li>';
    }
    modal_div += '</ul>';
    modal_div += '</div>';
	
	modal_div += '</div></div></div></div></div>';

	$("#deep_search_results").append(modal_div);

	$("#patent_"+id).modal("show");
}
