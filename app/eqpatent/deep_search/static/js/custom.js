(function($) {
	"use strict"

	///////////////////////////
	// Preloader
	$(window).on('load', function() {
		$("#preloader").delay(600).fadeOut();
	});

	///////////////////////////
	// Scrollspy
	$('body').scrollspy({
		target: '#nav',
		offset: $(window).height() / 2
	});

	///////////////////////////
	// Smooth scroll
	$("#nav .main-nav a[href^='#']").on('click', function(e) {
		e.preventDefault();
		var hash = this.hash;
		$('html, body').animate({
			scrollTop: $(this.hash).offset().top
		}, 600);
	});

	$('#back-to-top').on('click', function(){
		$('body,html').animate({
			scrollTop: 0
		}, 600);
	});

	///////////////////////////
	// Btn nav collapse
	$('#nav .nav-collapse').on('click', function() {
		$('#nav').toggleClass('open');
	});

	///////////////////////////
	// Mobile dropdown
	$('.has-dropdown a').on('click', function() {
		$(this).parent().toggleClass('open-drop');
	});

	///////////////////////////
	// magnificPopup
	$('.work').magnificPopup({
		delegate: '.lightbox',
		type: 'image'
	});

	///////////////////////////
	// Owl Carousel
	$('#about-slider').owlCarousel({
		items:1,
		loop:true,
		margin:15,
		nav: true,
		navText : ['<i class="fa fa-angle-left"></i>','<i class="fa fa-angle-right"></i>'],
		dots : true,
		autoplay : true,
		animateOut: 'fadeOut'
	});

	$('#testimonial-slider').owlCarousel({
		loop:true,
		margin:15,
		dots : true,
		nav: false,
		autoplay : true,
		responsive:{
			0: {
				items:1
			},
			992:{
				items:2
			}
		}
	});

	$(function() {


	    // This function gets cookie with a given name
	    function getCookie(name) {
	        var cookieValue = null;
	        if (document.cookie && document.cookie != '') {
	            var cookies = document.cookie.split(';');
	            for (var i = 0; i < cookies.length; i++) {
	                var cookie = jQuery.trim(cookies[i]);
	                // Does this cookie string begin with the name we want?
	                if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                    break;
	                }
	            }
	        }
	        return cookieValue;
	    }
	    var csrftoken = getCookie('csrftoken');

	    /*
	    The functions below will create a header with csrftoken
	    */

	    function csrfSafeMethod(method) {
	        // these HTTP methods do not require CSRF protection
	        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	    }
	    function sameOrigin(url) {
	        // test that a given url is a same-origin URL
	        // url could be relative or scheme relative or absolute
	        var host = document.location.host; // host + port
	        var protocol = document.location.protocol;
	        var sr_origin = '//' + host;
	        var origin = protocol + sr_origin;
	        // Allow absolute or scheme relative URLs to same origin
	        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
	            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
	            // or any other URL that isn't scheme relative or absolute i.e relative.
	            !(/^(\/\/|http:|https:).*/.test(url));
	    }

	    $.ajaxSetup({
	        beforeSend: function(xhr, settings) {
	            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
	                // Send the token to same-origin, relative URLs only.
	                // Send the token only if the method warrants CSRF protection
	                // Using the CSRFToken value acquired earlier
	                xhr.setRequestHeader("X-CSRFToken", csrftoken);
	            }
	        }
	    });

	});

})(jQuery);

var n_elements = {};
n_elements['independant_mandatory'] = 1;
n_elements['independant_optional'] = 1;
n_elements['dependant_mandatory'] = 1;
n_elements['dependant_optional'] = 1;

function add_row(id)
{
	var div = document.createElement('div');

	n_elements[id] += 1;
	var n = n_elements[id];
	
	html = '<div class="form-row">';
	html += '<div class="form-group col-lg-10">';
	html += '<input type="text" value="" class="form-control" name="';
	html += id;
	html += '_';
	html += n;
	html += '" id="';
	html += id;
	html += '_';
	html += n;
	html += '"></div></div>';

	div.innerHTML = html;

	document.getElementById(id+'_div').appendChild(div);

	$('#'+id+'_'+n).tagsInput({width:'auto', height:'60px', placeholder:''});
}

function add_row_after_back(div_id, idx)
{
	var div = document.createElement('div');

	html = '<div class="form-row">';
	html += '<div class="form-group col-lg-10">';
	html += '<input type="text" value="" class="form-control" name="';
	html += div_id;
	html += '_';
	html += idx;
	html += '" id="';
	html += div_id;
	html += '_';
	html += idx;
	html += '"></div></div>';

	div.innerHTML = html;

	document.getElementById(div_id+'_div').appendChild(div);

	$('#'+div_id+'_'+idx).tagsInput({width:'auto', height:'60px', placeholder:''});
}
