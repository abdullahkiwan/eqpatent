
var saved_comments = {}

function retrieve_comments(patent_id)
{
	if (patent_id in saved_comments)
		comment_txt = saved_comments[patent_id];
	else
		comment_txt = '';
	document.getElementById('comment_'+patent_id).value = comment_txt;
}

function close_comment_window(patent_id)
{
	comment_close_btn = document.getElementById('comment_close_btn_'+patent_id);
	comment_close_btn.onclick = function() 
	{
		retrieve_comments(patent_id);
	}
}

function save_comment_window(patent_id) {
	comment_save_btn = document.getElementById('comment_save_btn_'+patent_id);
	
	comment_save_btn.onclick = function() {
		comment_txt = document.getElementById('comment_'+patent_id).value;
		saved_comments[patent_id] = comment_txt;

		document.getElementById('comment_txt_'+patent_id).innerHTML = comment_txt.substring(0, 50)+'...';
	}
}

function create_comment_window(patent_id, patent_title) {

	if ( ! $('#comment_window_'+patent_id).length )
	{
		var modal_div = '';

		modal_div += '<div class="modal fade" id="comment_window_';
		modal_div += patent_id;
		modal_div += '" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">';
		modal_div += '<div class="modal-dialog modal-lg">';
		modal_div += '<div class="modal-content">';
		modal_div += '<div class="modal-header">';
		modal_div += '<h4 style="width: 500px;" class="modal-title" id="formModalLabel">';
		modal_div += patent_title
		modal_div += '</h4>';
		modal_div += '<div>';
		modal_div += '<button type="button" class="btn btn-light closeModal" data-dismiss="modal" #closeBtn id="comment_close_btn_';
		modal_div += patent_id;
		modal_div += '" name="comment_close_btn_';
		modal_div += patent_id;
		modal_div += '" style="margin-right: 10px;">Close</button>';

		modal_div += '<button type="button" class="btn btn-primary" id="comment_save_btn_';
		modal_div += patent_id;
		modal_div += '" name="comment_save_btn_';
		modal_div += patent_id;
		modal_div += '">Save Changes</button>';
		modal_div += '</div></div>';

		modal_div += '<div class="modal-body">';

		modal_div += '<div class="form-group row">';
		modal_div += '<label class="col-sm-2 text-left text-sm-right mb-0">Comment</label>';
		modal_div += '<div class="col-sm-10">';
		modal_div += '<textarea rows="5" class="form-control" placeholder="Type your comment..." id="comment_';
		modal_div += patent_id;
		modal_div += '"></textarea>';
		modal_div += '</div></div>';

		modal_div += '</div></div></div></div>';

		$("#deep_search_results").append(modal_div);

		$("#comment_window_"+patent_id).modal("show");

		comment_window = document.getElementById('comment_window_'+patent_id);

		comment_close_btn = document.getElementById('comment_close_btn_'+patent_id);
		comment_save_btn = document.getElementById('comment_save_btn_'+patent_id);

		save_comment_window(patent_id);
		close_comment_window(patent_id);
	}
	else
	{
		if (patent_id in saved_comments)
		{
			document.getElementById('comment_'+patent_id).value = saved_comments[patent_id];
		}
		$("#comment_window_"+patent_id).modal("show");
	}
}

function generate_report() {

	$("#notifications").empty();
	
	var request_data = [];

	if ($("input:checkbox:checked").length > 0)
	{
	    $("input:checkbox").each(function(){
		    var $this = $(this);

		    if($this.is(":checked")){

		    	var id = $this.attr("id").split('_')[1];
		    	var comment_element = document.getElementById("comment_"+id);
		    	
		    	var comment = '';
		    	if (comment_element)
			    	comment = comment_element.value;

			    request_data.push({"id":id, "comment":comment})
		    }
		});

	    request_data = JSON.stringify(request_data);
	    
	    console.log("Generating report is working!"); // sanity check
	    $.ajax({
	        url : "generate_report/", // the endpoint
	        type : "POST", // http method
	        dataType: 'json',
	        data : request_data, // data sent with the post request

	        // handle a successful response
	        success : function(data) {
				console.log("Report generated successfully");
				if (data.success)
				{
					var success_alert = '<div class="alert alert-success">';
					success_alert += 'Report was saved under : ';
					success_alert += data.output;
					success_alert += '</div>';

					$("#notifications").append(success_alert);
				}
				else
				{
					var success_alert = '<div class="alert alert-danger">';
					success_alert += 'Error occurred while generating the report.';
					success_alert += '</div>';

					$("#notifications").append(success_alert);
				}
	        }
	    });
	} 
	else
	{
		console.log("NO CHECKED PATENTS");

		var success_alert = '<div class="alert alert-warning">';
		success_alert += 'At least one of the patents has to be checked.';
		success_alert += '</div>';

		$("#notifications").append(success_alert);
	}
}
