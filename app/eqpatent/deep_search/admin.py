from django.contrib import admin
from .models import Patent, Claim, UserTracking
from .models import Word, SynonymPair, GeneralTypePair, SpecialTypePair

# Register your models here.
#admin.site.register(User)
admin.site.register(Patent)
admin.site.register(Claim)
admin.site.register(Word)
admin.site.register(SynonymPair)
admin.site.register(GeneralTypePair)
admin.site.register(SpecialTypePair)
admin.site.register(UserTracking)