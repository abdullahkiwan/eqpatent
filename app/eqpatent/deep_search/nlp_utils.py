
import json
import nltk
from nltk.corpus import wordnet as wn

from .models import Claim, Patent

def parse(claim):
    
    '''Extract nouns from english claim
        returns list'''

    tokens = nltk.word_tokenize(claim)
    tokens_pos_tagged = nltk.pos_tag(tokens)
    nouns = [word for word,pos in tokens_pos_tagged if pos == "NN"]

    ns = set()
    for word in nouns:
        ns.add(word)
    ns = list(ns)

    return ns


def sort_claims(claims):

    import operator

    claims_dict = {}
    for claim in claims:
        claims_dict[int(claim.split('.')[0])] = claim

    sorted_claims_dict = sorted(claims_dict.items(), key=operator.itemgetter(0))
   
    sorted_claims_list = []
    for claim in sorted_claims_dict:
        sorted_claims_list.append(claim[1])

    return sorted_claims_list


def get_wordnet_synonyms(word):
    
    synonyms = set() # sets have the benefit of containing no duplicate elements
    for meaning in wn.synsets(word): # words have different synsets/meanings inside wordnet
        lemma_names = [lemma.name() for lemma in meaning.lemmas()] # gets the synonyms
        for syn in lemma_names:

            if syn != word:
                syn = syn.replace('_', ' ')
                synonyms.add(syn)

    return list(synonyms)

def get_api_synonyms(word):
    import httplib2
    h = httplib2.Http()

    endpoint = 'https://wordsapiv1.p.mashape.com/words/'
    if len(word.split()) == 1:
        endpoint += word
    else:
        endpoint += '%20'.join(word.split())

    headers = {
        'X-RapidAPI-Host':'wordsapiv1.p.rapidapi.com',
        'X-RapidAPI-Key':'520ffb5a55msh4435be9278b9672p149105jsncfd1f963336c',
        "Accept": "application/json"
    }
    respond, data = h.request(endpoint,"GET",headers = headers)
    data = data.decode('utf-8') # Decode using the utf-8 encoding
    data = json.loads(data)

    synonyms = []
    general_types = []
    special_types = []

    if 'results' in data:
        for result in data['results']:
            if 'synonyms' in result: synonyms += result['synonyms']
            if 'typeOf' in result: general_types += result['typeOf']
            if 'hasTypes' in result: special_types += result['hasTypes']

    return {
            'synonyms':synonyms,
            'general_types':general_types,
            'special_types':special_types
            }
