from django.conf.urls import url

from . import views

urlpatterns = [
   
    url(r'^$', views.deep_search_index, name='deep_search'),
    
    url(r'^find_synonyms/$', views.find_synonyms),
    url(r'get_patent/$', views.get_patent),
    url(r'generate_report/$', views.generate_report),

    url(r'results/', views.search_view, name='deep_search_submit'),
    url(r'logout/', views.logout_user, name='logout'),
]