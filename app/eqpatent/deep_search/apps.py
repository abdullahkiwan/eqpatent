from django.apps import AppConfig


class DeepSearchConfig(AppConfig):
    name = 'deep_search'
