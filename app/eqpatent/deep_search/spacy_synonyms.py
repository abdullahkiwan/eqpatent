
import spacy
import operator

def get_spacy_synonyms(word):
	

	model = 'en_core_web_lg'

	nlp = spacy.load(model)

	vocab = list(nlp.vocab.strings)   

	print(word)

	word = nlp.vocab[word]

	print(word)

	similarities = []
	for w in word.vocab:

		if w.is_lower == word.is_lower and w.prob >= -15:
			if w.lower_ != word.lower_:
				sim = word.similarity(w)
				if sim > 0: similarities.append((w.lower_, sim))

	similarities.sort(key=operator.itemgetter(1), reverse=True)
	similarities = similarities[:10]
	synonyms = [w[0] for w in similarities]

	return synonyms