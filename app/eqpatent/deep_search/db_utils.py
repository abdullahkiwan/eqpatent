
from django.shortcuts import get_object_or_404
from django.db.models import Q

from .models import SynonymPair, Word, GeneralTypePair, SpecialTypePair
from .nlp_utils import get_wordnet_synonyms

import itertools

def get_db_synonyms(word):
	
	synonym_list = []
	try:
		synonym_pairs = SynonymPair.objects.filter(Q(word_1=word) | Q(word_2=word))

		for synonym_obj in synonym_pairs:
			if synonym_obj.word_1 != word: 
				synonym_list.append(synonym_obj.word_1)
			else: 
				synonym_list.append(synonym_obj.word_2)
		return list(set(synonym_list))
	except:
		return []

def get_db_general_types(word):
	
	general_types_list = []
	try:
		general_type_pairs = GeneralTypePair.objects.filter(Q(word_1=word) | Q(word_2=word))

		for type_obj in general_type_pairs:
			if type_obj.word_1 != word: general_types_list.append(type_obj.word_1)
			else: general_types_list.append(type_obj.word_2)
		
		return list(set(general_types_list))
	except:
		return []

def get_db_special_types(word):
	
	special_types_list = []
	try:
		special_type_pairs = SpecialTypePair.objects.filter(Q(word_1=word) | Q(word_2=word))

		for type_obj in special_type_pairs:
			if type_obj.word_1 != word: special_types_list.append(type_obj.word_1)
			else: special_types_list.append(type_obj.word_2)
		
		return list(set(special_types_list))
	except:
		return []

def add_db_synonyms(word, additional_synonyms, keywords_list):

	if not additional_synonyms: return

	for syn in additional_synonyms:
		syn = syn.lower()
		syn = syn.replace('_', ' ')
		synonym_pair = SynonymPair(word_1=word, word_2=syn, keywords=keywords_list)
		synonym_pair.save()

	syn_pairs = list(itertools.combinations(additional_synonyms, 2))
	for syn_pair in syn_pairs:
		synonym_pair = SynonymPair(word_1=syn_pair[0], word_2=syn_pair[1], keywords=keywords_list)
		synonym_pair.save()

def add_db_general_types(word, additional_general_types, keywords_list):
	
	if not additional_general_types: return

	for typ in additional_general_types:
		typ = typ.lower()
		typ = typ.replace('_', ' ')
		general_type_pair = GeneralTypePair(word_1=word, word_2=typ, keywords=keywords_list)
		general_type_pair.save()

	type_pairs = list(itertools.combinations(additional_general_types, 2))
	for type_pair in type_pairs:
		general_type_pair = GeneralTypePair(word_1=type_pair[0], word_2=type_pair[1], keywords=keywords_list)
		general_type_pair.save()	

def add_db_special_types(word, additional_special_types, keywords_list):

	if not additional_special_types: return

	for typ in additional_special_types:
		typ = typ.lower()
		typ = typ.replace('_', ' ')
		special_type_pair = SpecialTypePair(word_1=word, word_2=typ, keywords=keywords_list)
		special_type_pair.save()	

	type_pairs = list(itertools.combinations(additional_special_types, 2))
	for type_pair in type_pairs:
		special_type_pair = SpecialTypePair(word_1=type_pair[0], word_2=type_pair[1], keywords=keywords_list)
		special_type_pair.save()

def delete_db_synonyms(word):
	Word.objects.filter(text=word).delete()


